#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class TestProcess
  attr_reader :rakefile

  def initialize(rakefile)
    @rakefile = rakefile

    @rd, @wr = IO.pipe
    Dir.chdir(File.dirname(rakefile)) {
      @pid = Process.spawn('rake', '-f', File.basename(rakefile), out: @wr, err: @wr)
    }

    ObjectSpace.define_finalizer(self, proc { @rd.close })
  end

  def wait
    if @pid
      _, @status = Process.wait2(@pid)
      @pid = nil
      @wr.close
    end
  rescue Errno::ECHILD
    @pid = nil
    @wr.close
  end

  def ok?
    wait
    @status.exitstatus == 0
  end

  def output
    wait
    @output ||= @rd.read
  end
end

class Format
  def initialize maxLen
    @maxLen = maxLen
  end

  def format process
    "| %-#{@maxLen}s #{process.ok? ? 'OK' : 'FAILED'}" % process.rakefile
  end
end

desc('runs all tests')
task(:ut, [:patern]) { |_, opts|
  status =
    begin
      patern = opts[:patern] || '*'
      processes = Dir["#{patern}/**/rakefile.rb"].collect { |rake|
        TestProcess.new(rake)
      }

      f = Format.new(processes.collect { |p| p.rakefile.size }.max)

      processes.each { |p|
        $stdout.puts f.format(p) if p.ok?
      }

      processes.reject { |p|
        p.ok?
      }.each { |p|
        $stdout.puts f.format(p)
        $stderr.print p.output
      }

      processes.all? { |p| p.ok? }
    rescue Exception
      processes.each(&:wait)
      raise
    end

  abort('One of test suites failed!') unless status
}

task(:default => :ut)
