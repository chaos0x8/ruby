#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/MoveAndLink'

class MoveAndLinkTestSuite < Test::Unit::TestCase
    include ShellScripts

    def self.setupExistingSource
        Proc.new {
            setup {
                File.expects(:exists?).with(@source).returns(true).at_least(0)
            }
        }
    end

    def self.setupExistingDestination
        Proc.new {
            setup {
                File.expects(:directory?).with(@destination).returns(true).at_least(0)
            }
        }
    end

    context('MoveAndLink') {
        setup {
            File.expects(:exists?).returns(false).at_least(0)
            File.expects(:directory?).returns(false).at_least(0)

            @source = '/tmp/file1'
            @destination = '/tmp/directory/'
        }

        context('both source and destination exists') {
            merge_block(&setupExistingSource)
            merge_block(&setupExistingDestination)

            should('exec move and link when source and destination exists') {
                FileUtils.expects(:mv).with(@source, @destination)
                FileUtils.expects(:ln_s).with("/tmp/directory/file1", File.dirname(@source))

                moveAndLink @source, @destination
            }
        }

        { 'source' => setupExistingSource,
          'destination' => setupExistingDestination }.each { |name, set|
            context("with only existing #{name}") {
                merge_block(&set)

                should('raise when source or destination doesn\'t exists') {
                    assert_raise(RuntimeError) {
                        moveAndLink @source, @destination
                    }
                }
            }
        }
    }
end
