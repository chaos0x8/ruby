#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/DownloadManager'

autoload :SecureRandom, 'securerandom'
autoload :FileUtils, 'fileutils'

class TestDownloadManager < Test::Unit::TestCase
  def readFn
    File.open(@fn, 'r') { |f| f.read }
  end

  def writeFn *data
    File.open(@fn, 'w') { |f|
      data.each { |line|
        f.write line
      }
    }

    File.expects(:exist?).returns(true).with(@fn).at_least(0)
  end

  def parseLink url
    writeFn("#{url} dst\n")

    DownloadManager.parse(@fn).collect { |url, fn|
      File.extname(fn)
    }.join(',')
  end

  context('TestDownloadManager') {
    setup {
      @fn = "/tmp/#{SecureRandom.hex}_DM-input.txt"

      File.expects(:exist?).returns(false).at_least(0)
      File.expects(:size).returns(0).at_least(0)
      File.expects(:rm).never

      @st = mock
      @st.expects(:exitstatus).returns(1).at_least(0)

      Process.expects(:wait2).returns([42, @st]).at_least(0)
      Process.expects(:spawn).never

      $stdout.expects(:print).at_least(0)
      $stdout.expects(:puts).at_least(0)
    }

    teardown {
      FileUtils.rm @fn if File.exist? @fn
    }

    context('empty') {
      should('return empty data set') {
        assert_equal([], DownloadManager.parse(@fn).to_a)
      }
    }

    context('with data') {
      setup {
        writeFn(
          "url_xxx.ext1 target\n",
          "url_yyy.ext2&garbage target with spaces\n",
          "url_zzz.ext3 target    \n")

        @expected = [
          ['url_xxx.ext1', 'target.ext1'],
          ['url_yyy.ext2&garbage', 'target with spaces.ext2'],
          ['url_zzz.ext3', 'target.ext3']
        ]
      }

      should('return elements') {
        assert_equal(@expected, DownloadManager.parse(@fn).to_a)
      }

      should('yield elements') {
        actual = []

        DownloadManager.parse(@fn) { |src, dst|
          actual << [src,dst]
        }

        assert_equal(@expected, actual)
      }

      should('.wget download when file doesn\'t exist') {
        state = states('wget').starts_as('no')

        File.expects(:exist?).when(state.is('no')).with('dst').returns(false).at_least(0)
        File.expects(:exist?).when(state.is('yes')).with('dst').returns(true).at_least(0)
        File.expects(:size).when(state.is('yes')).with('dst').returns(2048).at_least(0)

        Process.expects(:spawn).with { |a0, **args| a0 == 'wget' }.then(state.is('yes'))
        @st.expects(:exitstatus).returns(0).at_least(0)

        assert_equal(true, !! DownloadManager.wget('src', 'dst'))
      }

      should('.wget do nothing when file is ok') {
        File.expects(:exist?).with('dst').returns(true).at_least(0)
        File.expects(:size).with('dst').returns(2048).at_least(0)

        assert_equal(false, !! DownloadManager.wget('src', 'dst'))
      }

      should('.cleanup do nothing when nothing was successfully downloaded') {
        DownloadManager.cleanup @fn

        expected =
          "url_xxx.ext1 target\n" +
          "url_yyy.ext2&garbage target with spaces\n" +
          "url_zzz.ext3 target    \n"

        assert_equal(expected, readFn)
      }

      should('.cleanup remove from file already downloaded files which seems to be corrent') {
        File.expects(:exist?).with('target with spaces.ext2').returns(true).at_least(0)
        File.expects(:size).with('target with spaces.ext2').returns(2048).at_least(0)

        File.expects(:exist?).with('target.ext3').returns(true).at_least(0)

        DownloadManager.cleanup @fn

        expected =
          "url_xxx.ext1 target\n" +
          "url_zzz.ext3 target\n"

        assert_equal(expected, readFn)
      }
    }

    context('with invalid data') {
      setup {
        writeFn("url_xxx.ext1\n")
      }

      should('raise') {
        assert_raise(RuntimeError) {
          DownloadManager.parse(@fn).to_a
        }
      }
    }

    context('strange url') {
      {
        'http://s3.xxx.net/N/gdhgahsdgh-fsdf-fdsf-adssad-19.flv?st=hvyapcxs64cxva7tggcypq&e=1498586712&start=0' => '.flv'
      }.each_with_index { |item, i|
        should("extract extension from url/#{i}") {
          url = item[0]
          expectedExt = item[1]

          assert_equal(expectedExt, parseLink(url))
        }
      }
    }
  }
end
