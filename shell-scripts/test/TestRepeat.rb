#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'TestSupport'

require_relative '../lib/Repeat'

class TestRepeat < Test::Unit::TestCase
  include ShellScripts

  context('TestRepeat') {
    setup {
      Process.prevent(:spawn, :wait, :kill)
      $stdin.prevent(:each_line)
    }

    should('do nothing') {
      repeat 0, 'echo', 'lol'
    }

    should('repeat given command') {
      [ 10, 11, 12 ].each { |pid|
        Process.expects(:spawn).with('echo', 'lol').returns(pid)
        Process.expects(:wait).with(pid)
      }

      repeat '3', 'echo', 'lol'
    }

    should('repeat given command from stdin') {
      $stdin.expects(:tty?).returns(false).at_least(0)
      $stdin.expects(:each_line).multiple_yields("xxx yyy\n", "zzz\n")

      Process.expects(:spawn).with('echo', 'xxx', 'yyy', 'zzz').returns(7)
      Process.expects(:wait).with(7)

      repeat '1', 'echo'
    }

    should('abort other executions') {
      seq = sequence('process')

      Process.expects(:spawn).in_sequence(seq).returns(10)
      Process.expects(:wait).in_sequence(seq).with(10)
      Process.expects(:spawn).in_sequence(seq).returns(11)
      Process.expects(:wait).in_sequence(seq).with(11).raises(Interrupt)
      Process.expects(:kill).in_sequence(seq).with('INT', 11)

      assert_raise(Interrupt) {
        repeat '3', 'echo', 'lol'
      }
    }
  }
end

