#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/ConditionalLogger'

class TestConditionalLogger < Test::Unit::TestCase
  context('ConditionalLogger') {
    setup {
      @out = mock
      @out.expects(:puts).never

      @seq = sequence('output')
    }

    should('not log when disabled') {
      ConditionalLogger.do(enabled: false, out: @out) { |logger|
        logger.print 'hello'
        logger.puts 'world'
      }
    }

    should('log one line') {
      @out.expects(:puts).with('hello world')

      ConditionalLogger.do(enabled: false, out: @out) { |logger|
        logger.print 'hello'
        logger.puts ' world'

        logger.enable
      }
    }

    should('many lines with one empty') {
      @out.expects(:puts).in_sequence(@seq).with('hello')
      @out.expects(:puts).in_sequence(@seq).with('')
      @out.expects(:puts).in_sequence(@seq).with('world')

      ConditionalLogger.do(enabled: false, out: @out) { |logger|
        logger.puts 'hello'
        logger.puts ''
        logger.print 'wor'
        logger.puts 'ld'

        logger.enable
      }
    }
  }
end
