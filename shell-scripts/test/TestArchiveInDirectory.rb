#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/ArchiveInDirectory'

class TestArchiveInDirectory < Test::Unit::TestCase
    include ShellScripts

    context('TestArchiveInDirectory') {
        setup {
            File.expects(:directory?).returns(false).at_least(0)
            File.expects(:symlink?).returns(false).at_least(0)
            FileUtils.expects(:mv).never
            FileUtils.expects(:mkdir).never

            @file1 = 'file1'
            @file2 = 'file2'
            @dir = '2016-06-08'

            @dateTime = mock()
            DateTime.expects(:now).returns(@dateTime).at_least(0)
            @dateTime.expects(:strftime).with('%Y-%m-%d').returns(@dir).at_least(0)
        }

        should('do nothing when no files found') {
            Dir.expects(:[]).with('*').returns(Array.new).at_least(0)

            archiveInDirectory
        }

        context('with one file found') {
            setup {
                Dir.expects(:[]).with('*').returns([@file1]).at_least(0)
            }

            [ :directory?, :symlink? ].each { |condition|
                should("not move '#{condition}'") {
                    File.expects(condition).with(@file1).returns(true).at_least(0)

                    archiveInDirectory
                }
            }

            context('with existing directory') {
                setup {
                    File.expects(:directory?).with(@dir).returns(true).at_least(0)
                }

                should('not create dir') {
                    FileUtils.expects(:mv).at_least(1)

                    archiveInDirectory
                }
            }
        }

        context('with two files found') {
            setup {
                Dir.expects(:[]).with('*').returns([@file1, @file2]).at_least(0)
            }

            should('create directory and move regular files') {
                FileUtils.expects(:mkdir).with(@dir)
                FileUtils.expects(:mv).with(@file1, @dir)
                FileUtils.expects(:mv).with(@file2, @dir)

                archiveInDirectory
            }
        }
    }
end
