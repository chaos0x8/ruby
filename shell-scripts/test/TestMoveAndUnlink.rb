#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/MoveAndUnlink'

class MoveAndLinkTestSuite < Test::Unit::TestCase
    include ShellScripts

    context('MoveAndUnlink') {
        setup {
            @source = 'file1'
            @target = 'file2'

            File.expects(:symlink?).with(@source).returns(true).at_least(0)
            File.expects(:readlink).with(@source).returns(@target).at_least(0)
            File.expects(:exists?).with(@target).returns(true).at_least(0)
        }

        should('remove link and move target in it\'s place') {
            self.expects(:system).with('unlink', 'file1')
            FileUtils.expects(:mv).with(@target, @source)

            moveAndUnlink(@source)
        }

        should('do nothing when source is not symlink') {
            File.expects(:symlink?).with(@source).returns(false).at_least(0)

            moveAndUnlink(@source)
        }

        should('only remove link when target doesn\'t exists') {
            File.expects(:exists?).with(@target).returns(false).at_least(0)
            self.expects(:system).with('unlink', 'file1')

            moveAndUnlink(@source)
        }
    }
end
