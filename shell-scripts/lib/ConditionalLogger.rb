#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class ConditionalLogger
  def self.do *args, **opts
    logger = ConditionalLogger.new(*args, **opts)

    yield logger

    logger.finish
  end

  def initialize enabled: true, out: $stdout
    @data = []
    @out = out
    @enabled = enabled
  end

  def print text
    @data[_last] ||= ''
    @data[_last] = @data[_last] + text
  end

  def puts text
    self.print text
    @data << nil
  end

  def enable
    @enabled = true
  end

  def finish
    if @enabled
      @data.each { |line|
        @out.puts line if line
      }
      @data = []
    end
  end

private
  def _last
    @data.empty? ? 0 : @data.size-1
  end
end
