#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

module DownloadManager
  module Detail
    def ok? fileName
      File.exist?(fileName) and File.size(fileName) > 1024
    end

    module_function :ok?
  end

  def parse fileName, &block
    Enumerator.new { |e|
      File.open(fileName, 'r') { |f|
        f.each_line { |line|
          if m = line.match(/^(\S+)\s+(\S.*?)\s*$/)
            src = m[1]
            ext = File.extname(src).sub(/^(\.\w+).*?$/, '\\1')
            dst = "#{m[2]}#{ext}"
            e << [src, dst]
          elsif not line.strip.empty?
            raise 'Could not parse!'
          end
        }
      } if File.exist? fileName
    }.each(&block)
  end

  def wget src, dst, logger: $stdout
    unless Detail.ok?(dst)
      begin
        logger.print "#{src} -> #{dst} ... "
        _, st = Process.wait2(Process.spawn('wget', src, '-O', dst, '-q'))
        if st.exitstatus == 0 and Detail.ok? dst
          logger.puts 'OK'
          true
        else
          logger.puts 'FAIL'
          FileUtils.rm dst if File.exist? dst
          false
        end
      rescue Exception
        logger.puts 'ERROR'
        FileUtils.rm dst if File.exist? dst
        raise
      end
    end
  end

  def cleanup fileName
    res = parse(fileName).to_a
    if res.any? { |src, dst| Detail.ok?(dst) }
      File.open(fileName, 'w') { |f|
        left = res.reject { |src, dst| Detail.ok?(dst) }
        left.each { |src, dst|
          extLen = File.extname(dst).size
          f.write "#{src} #{dst[0..dst.size-extLen-1]}\n"
        }
      }
    end
  end

  module_function :parse, :wget, :cleanup
end
