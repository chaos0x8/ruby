#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'open3'

module Singlefox
  class Window
    attr_reader :id, :title

    def initialize wmctrl_line
      @id, wmctrl_line = _word(wmctrl_line)
      _, wmctrl_line = _word(wmctrl_line)
      _, wmctrl_line = _word(wmctrl_line)
      @title = wmctrl_line.chomp
    end

    def == other
      _cmp == other._cmp
    end

    def close
      pid = Process.spawn('wmctrl', '-ic', @id)
    ensure
      Process.wait(pid)
    end

  protected
    def _cmp
      [ @id ]
    end

  private
    def _word line
      if it_start = line.index(/\s/)
        it_end = line.index(/\S/, it_start)
        w = line[0..it_start-1]
        [ w, line[it_end..-1] ]
      else
        [ line, '' ]
      end
    end
  end

  def findFirefox
    windows.find { |x| x.title.match(FIREFOX_TITLE_PATERN) }
  end

  def windows &block
    Enumerator.new { |e|
      out, st = Open3.capture2e('wmctrl', '-l')
      if st.exitstatus == 0
        out.each_line { |line|
          begin
            e << Window.new(line)
          rescue Exception => e
            $stderr.puts "Could not parse: #{line}"
            $stderr.puts e.to_s
            $stderr.puts e.backtrace.join("\n")
          end
        }
      end
    }.each(&block)
  end

  FIREFOX_TITLE_PATERN = /Mozilla Firefox/
end

include Singlefox

begin
  firefox = findFirefox

  Process.detach(Process.spawn('firefox', out: '/dev/null', err: '/dev/null')) unless firefox

  begin
    sleep 0.2
    firefox = findFirefox
  end until firefox

  while windows.include?(firefox)
    windows.select { |win|
      win.title.match(FIREFOX_TITLE_PATERN)
    }.reject { |win|
      win == firefox
    }.each { |win|
      win.close
    }

    sleep 2.0
  end
rescue Interrupt
  puts
  nil
end

