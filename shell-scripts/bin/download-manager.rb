#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

gem 'include'

require_relative '../lib/DownloadManager'
require_relative '../lib/ConditionalLogger'

require 'optparse'

autoload :UNIXServer, 'socket'
autoload :FileUtils, 'fileutils'
autoload :ShellUtils, 'ShellUtils'
autoload :Shellwords, 'shellwords'

NOTIFICATION = '/home/krzychu/bin/notification'

opts = {}

OptionParser.new { |op|
  op.banner = "Usage: #{File.basename(__FILE__)} [options] url_file"

  op.on('--dir=DIR', 'change dir before download') { |dir|
    opts[:dir] = dir
  }

  op.on('--help', 'prints help') {
    $stdout.puts op
    exit
  }
}.parse!

opts[:dir] ||= FileUtils.pwd

if ARGV[0]
  srvFn = '/tmp/download-manager.rb.sock'

  begin
    srv = UNIXServer.open(srvFn)

    begin
      ConditionalLogger.do(enabled: false) { |logger|
        Dir.chdir(opts[:dir]) {
          DownloadManager.parse(ARGV[0]) { |src, dst|
            if DownloadManager.wget(src, dst, logger: logger) and File.exist?(NOTIFICATION)
              FileUtils.touch(dst)

              Process.wait(Process.spawn(NOTIFICATION, 'Download Finished!', "#{opts[:dir]}/#{dst}", '-lsmplayer', '-a' "/usr/bin/smplayer #{opts[:dir]}/#{dst}"))
            else
              logger.enable
            end
          }
          DownloadManager.cleanup(ARGV[0])
        }
      }
    ensure
      srv.close
      FileUtils.rm srvFn if File.exist? srvFn
    end
  rescue Errno::EADDRINUSE
    if $stderr.tty?
      $stderr.puts "socket file probably already exist! '#{srvFn}'"
      exit 1
    end
  end
end

exit 0
