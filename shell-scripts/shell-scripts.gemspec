Gem::Specification.new { |s|
    s.name = 'shell-scripts'
    s.version = '0.0.17'
    s.date = File.mtime(__FILE__).strftime('%Y-%m-%d')
    s.summary = "#{s.name} library"
    s.description = 'Addition shell commands'
    s.authors = [ 'chaos0x8' ]
    s.files = Dir['lib/*.rb', 'bin/*.rb']
    s.executables = Dir['bin/*.rb'].collect { |x| File.basename(x) }
    s.add_runtime_dependency 'include', '~> 0.6.6'
    s.add_runtime_dependency 'nokogiri', '~> 1.6'
}
