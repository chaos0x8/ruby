#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'Colorize'

module Git
  class Status
    def initialize value
      @value = value.chomp.split("\n")
      @statusLine = @value.first
      @modifications = @value[1..@value.size].reject { |x| x.match(/^\?\?/) }
      @unstaged = @value[1..@value.size].select { |x| x.match(/^\?\?/) }
    end

    def upToDate?
      ! @statusLine.match(/behind \d/)
    end

    def modified?
      !! @statusLine.match(/ahead \d/) || !! modifications
    end

    def to_s
      [ statusLine, modifications ].join(' ').strip
    end

  private
    def statusLine
      if upToDate? and not modified?
        return @statusLine.green
      else
        return @statusLine.red
      end
    end

    def modifications
      result = Enumerator.new { |e|
        if @modifications.size > 0
          e << 'Modifications'
        end

        if @unstaged.size > 0
          e << 'Unstaged'
        end
      }.to_a

      if result.size > 0
        return "(#{result.join(', ')})".yellow
      end

      nil
    end
  end
end
