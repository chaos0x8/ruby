Gem::Specification.new { |s|
  s.name        = 'repo-status'
  s.version     = '0.1.0'
  s.date        = File.mtime(__FILE__).strftime('%Y-%m-%d')
  s.summary     = "#{s.name} library"
  s.description = "Displays git posository status in current directory and subdirectories"
  s.authors     = ["chaos0x8"]
  s.files       = Dir['lib/*.rb', 'bin/*.rb']
  s.executables = Dir['bin/*.rb'].collect { |x| File.basename(x) }
  s.add_runtime_dependency 'include', '~> 0.4.0'
}
