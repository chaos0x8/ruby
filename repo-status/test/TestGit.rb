#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/Git'

class TestGit < Test::Unit::TestCase
    include Git

    context('TestGit') {
        setup {
            @repo1 = Object.new
            @repo2 = Object.new

            self.expects(:`).never
        }

        should('do nothing when no directories found') {
            self.expects(:`).returns('').at_least(0)

            assert_equal(Array.new, findRepositories)
        }

        should('create repository for each .git found') {
            @dirs = "dir1/.git\ndir2/.git\n"
            self.expects(:`).with { |x| x.match(/^find.*#{Regexp.quote("-name '.git'")}/) }.returns(@dirs).at_least(0)

            Git::Repository.expects(:new).with('dir1').returns(@repo1).at_least(0)
            Git::Repository.expects(:new).with('dir2').returns(@repo2).at_least(0)

            assert_equal([ @repo1, @repo2 ], findRepositories)
        }
    }
end

