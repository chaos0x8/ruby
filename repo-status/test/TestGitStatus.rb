#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/GitStatus'

class TestGitStatus < Test::Unit::TestCase
  def modifications; "   file1\nM  file2"; end
  def unstaged; "?? file3\n?? file4"; end

  context('TestGitStatus') {
    setup {
      @header = "some header\n"
      @ahead = "ahead 1\n"
      @behind = "behind 1\n"
    }

    should('be up to date when patern doesn\'t appear') {
      assert_equal(true, Git::Status.new(@header).upToDate?)
      assert_equal(true, Git::Status.new(@ahead).upToDate?)
    }

    should('not be up to date when patern appears') {
      assert_equal(false, Git::Status.new(@behind).upToDate?)
    }

    should('have modifications when patern appears') {
      assert_equal(true, Git::Status.new(@ahead).modified?)
    }

    should('have modifications when files are modifies') {
      assert_equal(true, Git::Status.new(modifications).modified?)
      assert_equal(true, Git::Status.new(unstaged).modified?)
    }

    should('not have modifications when patern doesn\'t appear') {
      assert_equal(false, Git::Status.new(@header).modified?)
      assert_equal(false, Git::Status.new(@behind).modified?)
    }

    should('have no modifications or unstaged') {
      @sut = Git::Status.new(@header)

      assert_not_match(/Modifications/, @sut.to_s)
      assert_not_match(/Unstaged/, @sut.to_s)
    }

    [ 'Modifications', 'Unstaged' ].each { |arg|
      should("have #{arg}") {
        symbol = arg.downcase

        @sut = Git::Status.new(@header + send(symbol))

        assert_match(arg, @sut.to_s)
      }
    }
  }
end
