#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/GitRepository'

class TestGitRepository < Test::Unit::TestCase
    context('TestGitRepository') {
        setup {
            @sut = Git::Repository.new('path')
            @status = Object.new

            Git::Status.expects(:new).with('status result').returns(@status).at_least(0)
            @sut.expects(:`).with('cd path && git status -b --porcelain').returns('status result').at_least(0)
        }

        should('create status') {
            Git::Status.expects(:new).with('status result').returns(@status)
            @sut.expects(:`).with('cd path && git status -b --porcelain').returns('status result')

            assert_equal(@status, @sut.status)
        }

        should('formats string') {
            @status.expects(:to_s).returns('status string').at_least(0)

            assert_match(/path.*status string/, @sut.to_s)
        }
    }
end

