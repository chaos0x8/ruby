#!/usr/bin/env ruby

require 'sqlite3'

require_relative 'Record'

module SQLiteUtils
  def escape_ text
    if text.kind_of? Numeric
      text.to_s
    else
      "'#{text.gsub("'", "''")}'"
    end
  end
end

class DefaultValue
  include SQLiteUtils

  def initialize value
    @value = value
  end

  def + text
    if @value
      "#{text} DEFAULT #{escape_(@value)}"
    else
      text
    end
  end
end

class FieldText
  def initialize name, default: nil
    @name_ = name
    @default = DefaultValue.new(default)
  end

  def to_s
    @default + "#{@name_} TEXT NOT NULL"
  end
end

class FieldInteger
  def initialize name, default: nil
    @name_ = name
    @default = DefaultValue.new(default)
  end

  def to_s
    @default + "#{@name_} INTEGER NOT NULL"
  end
end

class FieldRelation < FieldInteger

end

class FieldId
  def to_s
    'id INTEGER PRIMARY KEY AUTOINCREMENT'
  end
end

class SimpleSqLite3
  include SQLiteUtils

  def initialize fn
    @fn_ = fn
    @db_ = SQLite3::Database.new(@fn_)
  end

  def createTable name, fields:
    code = []
    code << "CREATE TABLE IF NOT EXISTS #{name}"
    code << "(#{fields.join(', ')})"
    @db_.execute(code.join(' '))
  end

  def insertInto table, **fields
    fields.select! { |k, v| !!v }

    code = []
    code << "INSERT INTO #{table}"
    code << "(#{fields.keys.join(', ')})"
    code << "VALUES (#{fields.values.collect { |x| escape_(x) }.join(', ')})"
    @db_.execute(code.join(' '))
  end

  def update table, fields:, where: {}
    code = []

    updates = fields.collect { |k, v|
      "#{k} = #{escape_(v)}"
    }.join(', ')

    code << "UPDATE #{table} SET #{updates}"
    code << where_(where)

    @db_.execute(code.reject(&:nil?).join(' '))
  end

  def select table, fields: [], where: {}
    code = []

    if fields.empty?
      code << "SELECT * FROM #{table}"
    else
      code << "SELECT #{fields.join(', ')} FROM #{table}"
    end

    code << where_(where)

    r = @db_.execute(code.reject(&:nil?).join(' '))

    if fields.empty?
      r
    else
      r.collect { |values|
        Record.new(Hash[fields.zip(values)])
      }
    end
  end

private
  def where_ fields
    if fields.size > 0
      conditions = fields.collect { |field, val| "#{field} = #{val}" }.join(' AND ')
      "WHERE #{conditions}"
    end
  end
end
