#!/usr/bin/env ruby

class Record
  def initialize fields
    registerFields(*fields.keys)

    fields.each { |field, value|
      instance_variable_set(:"@#{field}_", value)
    }
  end

  def read *fields
    fields.collect { |field|
      instance_variable_get(:"@#{field}_")
    }
  end

  def registerFields *fields
    fields.each { |field|
      (class << self; self; end).class_eval {
        define_method(:"#{field}") {
          instance_variable_get(:"@#{field}_")
        }

        define_method(:"#{field}=") { |value|
          instance_variable_set(:"@#{field}_", value)
        }
      }
    }
  end

  def registerReadOnlyFields **fields
    fields.each { |field, value|
      (class << self; self; end).class_eval {
        define_method(field) {
          value
        }
      }
    }
  end

  def registerMethod name, &block
    (class << self; self; end).class_eval {
      define_method(name) { |*args, **opts|
        block.call(self, *args, **opts)
      }
    }
  end
end
