Gem::Specification.new { |s|
  s.name        = 'simple-sqlite3'
  s.version     = '0.0.0'
  s.date        = File.mtime(__FILE__).strftime('%Y-%m-%d')
  s.summary     = "#{s.name} library"
  s.description = "Simple wrapper for sqlite3"
  s.authors     = ["chaos0x8"]
  s.files       = Dir['lib/**/*.rb']
}

