#!/usr/bin/env ruby

require 'rake/testtask'

Rake::TestTask.new(:test) { |t|
    t.pattern = "#{File.dirname(__FILE__)}/test/**/Test*.rb"
}

desc "#{File.basename(File.dirname(__FILE__))}"
task(:default => :test)

desc "builds gem file"
task(:gem => 'simple-sqlite3.gemspec') {
  sh 'gem build simple-sqlite3.gemspec'
  Dir['*.gem'].sort{ |a, b| File.mtime(a) <=> File.mtime(b) }[0..-2].each { |fn|
    FileUtils.rm(fn, verbose: true)
  }
}

