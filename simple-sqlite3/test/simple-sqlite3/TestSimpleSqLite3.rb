#!/usr/bin/env ruby

require 'minitest/autorun'
require 'shoulda'
require 'mocha/setup'

require_relative '../../lib/simple-sqlite3/SimpleSqLite3.rb'

class TestSimpleSqLite3 < Minitest::Test
  context('TestSimpleSqLite3') {
    setup {
      @db = mock
      SQLite3::Database.expects(:new).returns(@db)
      @sut = SimpleSqLite3.new('/tmp/test.sqlite3')
    }

    should('create table with text field') {
      @db.expects(:execute).with("CREATE TABLE IF NOT EXISTS Player (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL)")
      @sut.createTable 'Player', fields: [ FieldId.new, FieldText.new('name') ]
    }

    should('create table with integer field') {
      @db.expects(:execute).with("CREATE TABLE IF NOT EXISTS Player (id INTEGER PRIMARY KEY AUTOINCREMENT, age INTEGER NOT NULL)")
      @sut.createTable 'Player', fields: [ FieldId.new, FieldRelation.new('age') ]
    }

    should('create table with relation field') {
      @db.expects(:execute).with("CREATE TABLE IF NOT EXISTS Player (id INTEGER PRIMARY KEY AUTOINCREMENT, name_id INTEGER NOT NULL)")
      @sut.createTable 'Player', fields: [ FieldId.new, FieldRelation.new('name_id') ]
    }

    should('create table with default text') {
      @db.expects(:execute).with("CREATE TABLE IF NOT EXISTS Player (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL DEFAULT 'Kerry')")
      @sut.createTable 'Player', fields: [ FieldId.new, FieldText.new('name', default: 'Kerry') ]
    }

    should('create table with default integer') {
      @db.expects(:execute).with("CREATE TABLE IF NOT EXISTS Player (id INTEGER PRIMARY KEY AUTOINCREMENT, age INTEGER NOT NULL DEFAULT 42)")
      @sut.createTable 'Player', fields: [ FieldId.new, FieldInteger.new('age', default: 42) ]
    }

    should('insert into table') {
      @db.expects(:execute).with('INSERT INTO Player (name) VALUES (\'Kerry\')')
      @sut.insertInto('Player', name: 'Kerry')
    }

    should('insert into table without nil fields') {
      @db.expects(:execute).with('INSERT INTO Player (name, age) VALUES (\'Kerry\', 42)')
      @sut.insertInto('Player', name: 'Kerry', last_name: nil, age: 42, )
    }

    should('insert escaped into table') {
      @db.expects(:execute).with('INSERT INTO Player (name) VALUES (\'Kerry\'\'s\')')
      @sut.insertInto('Player', name: 'Kerry\'s')
    }

    should('select from table') {
      @db.expects(:execute).with('SELECT * FROM Player').returns([['1', 'Kerry']])
      assert_equal([['1', 'Kerry']], @sut.select('Player'))
    }

    should('select fields from table') {
      @db.expects(:execute).with('SELECT name FROM Player').returns([['Kerry']])

      r = @sut.select('Player', fields: ['name'])

      assert_equal(1, r.size)
      assert_equal('Kerry', r.first.name)
    }

    should('select fields from table where id match') {
      @db.expects(:execute).with('SELECT name FROM Player WHERE id = 1').returns([['Kerry']])

      r = @sut.select('Player', fields: ['name'], where: { id: 1 })

      assert_equal(1, r.size)
      assert_equal('Kerry', r.first.name)
    }

    should('update fields in table') {
      @db.expects(:execute).with('UPDATE Player SET name = \'Kerry\'')

      @sut.update('Player', fields: { name: 'Kerry' })
    }

    should('update integer field in table') {
      @db.expects(:execute).with('UPDATE Player SET age = 21')

      @sut.update('Player', fields: { age: 21 })
    }

    should('update fields in table with where') {
      @db.expects(:execute).with('UPDATE Player SET name = \'Kerry\' WHERE id = 5')

      @sut.update('Player', fields: { name: 'Kerry' }, where: { id: 5 })
    }
  }
end
