#!/usr/bin/env ruby

require 'minitest/autorun'
require 'shoulda'
require 'mocha/setup'


class TestSimpleSqLite3 < Minitest::Test
  context('TestSimpleSqLite3') {
    setup {
      require_relative '../lib/simple-sqlite3'
    }

    should('import Record') {
      assert(!! defined? Record)
    }

    should('import SQLiteUtils') {
      assert(!! defined? SQLiteUtils)
    }

    should('import DefaultValue') {
      assert(!! defined? DefaultValue)
    }

    should('import FieldText') {
      assert(!! defined? FieldText)
    }

    should('import FieldInteger') {
      assert(!! defined? FieldInteger)
    }

    should('import FieldRelation') {
      assert(!! defined? FieldRelation)
    }

    should('import FieldId') {
      assert(!! defined? FieldId)
    }

    should('import SimpleSqLite3') {
      assert(!! defined? SimpleSqLite3)
    }
  }
end

