#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

module ChangeWallpaper
    class WallpaperLog
        FILE_NAME = "#{ENV['HOME']}/.used-wallpapers.txt"

        def self.addWallpaper wallpaper
            begin
                f = File.open(FILE_NAME, "a")
                f.write "#{wallpaper}\n"
                f.close
            rescue
            end
        end

        def self.getUsedWallpapers
            begin
                f = File.open(FILE_NAME, "r")
                data = f.read
                f.close

                return data.split "\n"
            rescue
                return []
            end
        end

        def self.clearLog
            f = File.open(FILE_NAME, "w")
            f.close
        end
    end

    @@wallpapersCache = nil
    def self.wallpapers dir
        @@wallpapersCache ||= Dir["#{ENV['HOME']}/Obrazy/#{dir}/*"]
    end

    def self.wallpapersToUse dir
        wallpapers(dir) - WallpaperLog.getUsedWallpapers
    end

    def self.getCurrentWallpaper
        match = GSetting.getWallpaper.match(/^'file:\/\/(.*)'$/)
        if match.nil? or match[1].empty?
            raise RuntimeError.new "Cannot find current wallpaper"
        end
        match[1]
    end

    def self.deleteCurrentWallpaper
        system "gvfs-trash #{getCurrentWallpaper}"
    end

    def self.isAlreadySet? wallpaper
        begin
            return wallpaper == getCurrentWallpaper
        rescue RuntimeError
            return false
        end
    end

    def self.pickRandomWallpaper dir
        random = nil

        while random.nil?
            sample = wallpapersToUse(dir).sample

            unless isAlreadySet? sample
                random = sample
            end

            WallpaperLog.addWallpaper sample
        end

        random
    end

    def self.setRandomWallpaper dir
        case wallpapers(dir).size
        when 0
            raise RuntimeError.new("No images to pick")
        when 1
            GSetting.setWallpaper wallpapers[0] unless isAlreadySet? wallpapers[0]
        else
            WallpaperLog.clearLog if wallpapersToUse(dir).empty?
            GSetting.setWallpaper pickRandomWallpaper(dir)
        end
    end

    def self.setScaledOption
        GSetting.setValue GSetting::BACKGROUND_PATH, "picture-options",
            "scaled" unless GSetting.getValue(GSetting::BACKGROUND_PATH, "picture-options") == "'scaled'"
    end
end
