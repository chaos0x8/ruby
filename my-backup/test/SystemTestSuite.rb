#!/usr/bin/ruby

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/System'

class SystemTest < Test::Unit::TestCase
  context('SystemTest') {
    should('return single line nodename') {
      assert_equal(System::nodename.chomp, System::nodename)
    }
  }
end
