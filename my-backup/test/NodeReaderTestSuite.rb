#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/NodeReader'

class NodeReaderTestSuite < Test::Unit::TestCase
    def setFileContent nodeFile, *content
        mergedContent = Array.new
        mergedContent += content[0..content.size-2].collect{|x| x + "\n"} if content.size > 1
        mergedContent.push(content.last)

        file = Object.new
        File.expects(:exists?).with(nodeFile).returns(true)
        File.expects(:open).with(nodeFile, 'r').yields(file)
        file.expects(:each_line).multiple_yields(*mergedContent)
    end

    context('NodeReader') {
        setup {
            @nodeFile = '/tmp/nodefile.node'

            @backup = Object.new
            @sut = NodeReader.new
        }

        should('does nothing when node file doesn\'t exist') {
            File.expects(:exists?).with(@nodeFile).returns(false).at_least(0)

            @sut.initializeBackupFromNodeFile(@backup, @nodeFile)
        }

        context('with nodefile') {
            setup {
                File.expects(:exists?).with(@nodeFile).returns(true).at_least(0)
            }

            should('add sources for given node') {
                setFileContent(@nodeFile, 'nodename:node',
                                          'backup: destination:destination source:source',
                                          'backup: destination:destination2 source:source2')

                @backup.expects(:addSource).with('node', 'destination', 'source')
                @backup.expects(:addSource).with('node', 'destination2', 'source2')

                @sut.initializeBackupFromNodeFile(@backup, @nodeFile)
            }

            should('add excludes to backup') {
                setFileContent(@nodeFile, 'nodename:node',
                                          'backup: destination:dest source:src extra:(exclude:*.patern1 exclude:*.patern2)')

                @seq1, @seq2 = sequence('patern1'), sequence('patern2')
                @backup.expects(:addSource).with('node', 'dest', 'src').in_sequence(@seq1, @seq2)
                @backup.expects(:addSourceExclude).with('node', 'dest', 'src', '*.patern1').in_sequence(@seq1)
                @backup.expects(:addSourceExclude).with('node', 'dest', 'src', '*.patern2').in_sequence(@seq2)

                @sut.initializeBackupFromNodeFile(@backup, @nodeFile)
            }

            should('add includes to backup') {
                setFileContent(@nodeFile, 'nodename:node',
                                          'backup: destination:dest source:src extra:(include:*.patern1 include:*.patern2)')

                @seq1, @seq2 = sequence('patern1'), sequence('patern2')
                @backup.expects(:addSource).with('node', 'dest', 'src').in_sequence(@seq1, @seq2)
                @backup.expects(:addSourceInclude).with('node', 'dest', 'src', '*.patern1').in_sequence(@seq1)
                @backup.expects(:addSourceInclude).with('node', 'dest', 'src', '*.patern2').in_sequence(@seq2)

                @sut.initializeBackupFromNodeFile(@backup, @nodeFile)
            }

            should('add no archive destination') {
                setFileContent(@nodeFile, 'noArchive: destination:destination')

                @backup.expects(:addNoArchiveDetination).with('destination')

                @sut.initializeBackupFromNodeFile(@backup, @nodeFile)
            }

            should('ignore empty lines') {
                setFileContent(@nodeFile, 'nodename:node1',
                                          'backup: destination:dest1 source:source1',
                                          '',
                                          'nodename:node2',
                                          '',
                                          'backup: destination:dest2 source:source2')

                @backup.expects(:addSource).with('node1', 'dest1', 'source1')
                @backup.expects(:addSource).with('node2', 'dest2', 'source2')

                @sut.initializeBackupFromNodeFile(@backup, @nodeFile)
            }

            should('raise runtime error when syntax is wrong') {
                setFileContent(@nodeFile, 'sd dw fs gfer')

                assert_raise(RuntimeError) {
                    @sut.initializeBackupFromNodeFile(@backup, @nodeFile)
                }
            }

            should('raise runtime error when nodename is not present before backup') {
                setFileContent(@nodeFile, 'backup: destination:#{ENV[\'HOME\']} source:/home/#{ENV[\'USER\']}/dir1')

                assert_raise(RuntimeError) {
                    @sut.initializeBackupFromNodeFile(@backup, @nodeFile)
                }
            }

            should('replace env variables') {
                setFileContent(@nodeFile, 'nodename:node',
                                          'backup: destination:#{ENV[\'HOME\']} source:/home/#{ENV[\'USER\']}/dir1',
                                          'backup: destination:#{ENV[\'HOME\']} source:/home/#{ENV[\'USER\']}/dir2')

                @backup.expects(:addSource).with('node', ENV['HOME'], "#{ENV['HOME']}/dir1")
                @backup.expects(:addSource).with('node', ENV['HOME'], "#{ENV['HOME']}/dir2")

                @sut.initializeBackupFromNodeFile(@backup, @nodeFile)
            }
        }
    }
end
