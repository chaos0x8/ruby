#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/Backup'

class BackupTestSuite < Test::Unit::TestCase
    def self.shouldHaveNoEffect
        Proc.new {
            should('do nothing when storing backup') {
                @sut.storeOldBackup
            }

            should('do nothing when doing backup') {
                @sut.updateFiles
            }
        }
    end

    def self.baseExpects nodename
        Proc.new {
            setup {
                @nodename = nodename
                @destination = '/tmp/my-backup'
                @pid = 42

                System.expects(:nodename).returns(@nodename).at_least(0)
                File.expects(:exists?).with(anything).returns(false).at_least(0)
                File.expects(:exists?).with(@destination).returns(true).at_least(0)

                @sut = Backup.new
                @sut.expects(:puts).with(anything).at_least(0)
            }
        }
    end

    def self.manySources
        Proc.new {
            setup {
                @sut.addSource 'nodename_1', @destination, "/tmp/directory1"
                @sut.addSource 'nodename_1', @destination, "/tmp/subdir/directory2"
                @sut.addSource 'nodename_1', @destination, "/tmp/directory3"
                @sut.addSource 'nodename_2', @destination, "/tmp/directory4"
                @sut.addSource 'nodename_2', @destination, "/tmp/subdir/directory5"
                @sut.addSource 'nodename_2', @destination, "/tmp/subdir/directory6"
                @sut.addSource 'nodename_2', @destination, "/tmp/directory7"
            }
        }
    end

    def expectMd5 file, hex
        md5 = Object.new
        Digest::MD5.expects(:file).with(file).returns(md5).at_least(0)
        md5.expects(:hexdigest).returns(hex).at_least(0)
    end

    context('') {
        setup {
            Process.expects(:spawn).never
            Process.expects(:wait).never

            File.expects(:exists?).never
        }

        context("BackupTestSuite invalid nodename") {
            merge_block(&baseExpects('node'))
            merge_block(&manySources)

            merge_block(&shouldHaveNoEffect)
        }

        context("BackupTestSuite nodename_1 with 'dd' source mode") {
            merge_block(&baseExpects('nodename_1'))

            setup {
                @source = '/tmp/subdir/block_device'
                @sut.addSource @nodename, @destination, @source
                @sut.addSourceMode @nodename, @destination, @source, 'dd'
                File.expects(:exists?).with(@source).returns(true).at_least(0)

                @target = "#{@destination}/#{@nodename}/#{File.basename(@source)}"

                @seq = sequence('spawn sequence')
            }

            context('when target exists') {
                setup {
                    File.expects(:exists?).with(@target).returns(true).at_least(0)
                }

                should('do nothing') {
                    @sut.updateFiles
                }
            }

            context('when target doesn\'t exists') {
                setup {
                    @st = states('operation').starts_as('before')
                    File.expects(:exists?).with(@target).returns(false).when(@st.is('before')).at_least(0)
                    File.expects(:exists?).with(@target).returns(true).when(@st.is('after')).at_least(0)
                }

                should('create dd archive') {
                    Process.expects(:spawn).with("dd if=#{@source} of=#{@target} bs=1G conv=fsync", anything).
                        returns(@pid).then(@st.is('after')).in_sequence(@seq)
                    Process.expects(:wait).with(@pid).in_sequence(@seq)

                    @sut.updateFiles
                }

                should('remove partialy createad file when operation fails') {
                    Process.expects(:spawn).with(anything, anything).
                        returns(@pid).then(@st.is('after')).in_sequence(@seq)
                    Process.expects(:wait).with(@pid).in_sequence(@seq).raises(Interrupt)
                    Process.expects(:kill).with('TERM', @pid).in_sequence(@seq)
                    Process.expects(:wait).with(@pid).in_sequence(@seq)

                    FileUtils.expects(:rm).with(@target)

                    assert_raise(Interrupt) {
                        @sut.updateFiles
                    }
                }
            }
        }

        [ 'tar', '_unknown_' ].each { |mode|
            context("BackupTestSuite nodename_1 with '#{mode}' source mode") {
                merge_block(&baseExpects('nodename_1'))

                setup {
                    @source = '/tmp/subdir/directory2'
                    @sut.addSource @nodename, @destination, @source
                    @sut.addSourceMode @nodename, @destination, @source, mode if mode
                    File.expects(:exists?).with(@source).returns(true).at_least(0)
                }

                case mode
                when 'tar'
                    setup {
                        @target = "#{@destination}/#{@nodename}/#{File.basename(@source)}.tar.xz"
                        @tmpSourceTar = "/tmp/#{File.basename(@source)}.tar"
                        @tmpSource = "/tmp/#{File.basename(@source)}.tar.xz"
                    }

                    context('when process is interrupted') {
                        setup {
                            seq = sequence('interrupted spawn sequence')

                            Process.expects(:spawn).with(anything, anything).returns(@pid).in_sequence(seq)
                            Process.expects(:wait).with(@pid).raises(Interrupt).in_sequence(seq)
                            Process.expects(:kill).with('TERM', @pid).in_sequence(seq)
                            Process.expects(:wait).with(@pid).in_sequence(seq)
                        }

                        should('only raise when file was not yet created') {
                            assert_raise(Interrupt) {
                                @sut.updateFiles
                            }
                        }

                        should('remove file before exception is re raised') {
                            File.expects(:exists?).with(@tmpSource).returns(true).at_least(0)
                            FileUtils.expects(:rm).with(@tmpSource)

                            File.expects(:exists?).with(@tmpSourceTar).returns(true).at_least(0)
                            FileUtils.expects(:rm).with(@tmpSourceTar)

                            assert_raise(Interrupt) {
                                @sut.updateFiles
                            }
                        }
                    }

                    context('when process spaws sucessfuly') {
                        setup {
                            seq = sequence('spawn sequence')

                            Process.expects(:spawn).with("tar -cf #{@tmpSourceTar} #{@source}", [:out, :err] => '/dev/null').returns(@pid).in_sequence(seq)
                            Process.expects(:wait).with(@pid).in_sequence(seq)

                            Process.expects(:spawn).with("xz -z #{@tmpSourceTar}", [:out, :err] => '/dev/null').returns(@pid+1).in_sequence(seq)
                            Process.expects(:wait).with(@pid+1).in_sequence(seq)

                            expectMd5(@tmpSource, 'hex')
                        }

                        context('when target doesn\'t exists') {
                            setup {
                                File.expects(:exists?).with(@target).returns(false).at_least(0)
                            }

                            should('create tar archive') {
                                FileUtils.expects(:mv).with(@tmpSource, @target, :verbose => true)

                                @sut.updateFiles
                            }

                            should('print spawn command') {
                                FileUtils.expects(:mv).at_least(0)
                                @sut.expects(:puts).with{ |t| t.match(/spawn 'tar .*'/) }
                                @sut.expects(:puts).with{ |t| t.match(/spawn 'xz .*'/) }

                                @sut.updateFiles
                            }
                        }

                        context('when target exists') {
                            setup {
                                File.expects(:exists?).with(@target).returns(true).at_least(0)
                            }

                            should('create tar archive when target exists with different checksum') {

                                expectMd5(@target, 'other-hex')

                                FileUtils.expects(:mv).with(@tmpSource, @target, :verbose => true)

                                @sut.updateFiles
                            }

                            should('remove temporary archive when checksum is same') {
                                expectMd5(@target, 'hex')

                                File.expects(:exists?).with(@tmpSource).returns(true).at_least(0)
                                FileUtils.expects(:rm).with(@tmpSource)

                                @sut.updateFiles
                            }
                        }
                    }
                when '_unknown_'
                    should('raise') {
                        assert_raise(RuntimeError) {
                            @sut.updateFiles
                        }
                    }
                else
                    raise 'Logic Error'
                end
            }
        }

        context("BackupTestSuite nodename_1 with many sources") {
            merge_block(&baseExpects('nodename_1'))
            merge_block(&manySources)

            context('invalid destination') {
                setup {
                    File.expects(:exists?).with(@destination).returns(false).at_least(1)
                }

                merge_block(&shouldHaveNoEffect)
            }

            context('destination exists') {
                setup {
                    File.expects(:exists?).with(anything).returns(false).at_least(0)
                    File.expects(:exists?).with(@destination).returns(true).at_least(1)
                }

                should('not create archive with old backup when destination is excluded') {
                    @sut.addNoArchiveDetination(@destination)

                    @sut.storeOldBackup
                }

                should('exclude some files when exclude is specified') {
                    @sut.addSourceExclude 'nodename_1', @destination, "/tmp/subdir/directory2", '*.patern1'
                    @sut.addSourceExclude 'nodename_1', @destination, "/tmp/subdir/directory2", '*.patern2'

                    File.expects(:exists?).with("/tmp/subdir/directory2").returns(true).at_least(1)

                    Process.expects(:spawn).with("rsync -vrht --delete-before --delete-excluded --exclude \\*.patern1 --exclude \\*.patern2 /tmp/subdir/directory2 /tmp/my-backup/nodename_1").returns(@pid)
                    Process.expects(:wait).with(@pid)

                    @sut.updateFiles
                }

                should('include some files when exclude is specified') {
                    @sut.addSourceInclude 'nodename_1', @destination, "/tmp/subdir/directory2", '*.patern1'
                    @sut.addSourceInclude 'nodename_1', @destination, "/tmp/subdir/directory2", '*.patern2'

                    File.expects(:exists?).with("/tmp/subdir/directory2").returns(true).at_least(1)

                    Process.expects(:spawn).with("rsync -vrht --delete-before --delete-excluded --include \\*.patern1 --include \\*.patern2 /tmp/subdir/directory2 /tmp/my-backup/nodename_1").returns(@pid)
                    Process.expects(:wait).with(@pid)

                    @sut.updateFiles
                }

                should('add option when option is pecified') {
                    @sut.addSourceOption 'nodename_1', @destination, "/tmp/subdir/directory2", '--prune-empty-dirs'
                    @sut.addSourceOption 'nodename_1', @destination, "/tmp/subdir/directory2", '--another-option'

                    File.expects(:exists?).with("/tmp/subdir/directory2").returns(true).at_least(1)

                    Process.expects(:spawn).with("rsync -vrht --delete-before --delete-excluded --prune-empty-dirs --another-option /tmp/subdir/directory2 /tmp/my-backup/nodename_1").returns(@pid)
                    Process.expects(:wait).with(@pid)

                    @sut.updateFiles
                }

                should("do nothing when old backup doesn't exist") {
                    @sut.storeOldBackup
                }

                context(', backup already exists') {
                    setup {
                        File.expects(:exists?).with("/tmp/my-backup/nodename_1").returns(true).at_least(1)
                    }

                    should("create archive with old backup") {
                        Process.expects(:spawn).with("cd /tmp/my-backup && tar -cvf nodename_1.tar nodename_1").returns(@pid)
                        Process.expects(:wait).with(@pid)

                        @sut.storeOldBackup
                    }

                    should("update previous backup") {
                        File.expects(:exists?).with("/tmp/my-backup/nodename_1.tar").returns(true).at_least(1)

                        Process.expects(:spawn).with("cd /tmp/my-backup && tar -uvf nodename_1.tar nodename_1").returns(@pid)
                        Process.expects(:wait).with(@pid)

                        @sut.storeOldBackup
                    }
                }
            }
        }

        context('BackupTestSuite nodename2 with many sources') {
            merge_block(&baseExpects('nodename_2'))
            merge_block(&manySources)

            should("synchronize backup directories if exists") {
                File.expects(:exists?).with("/tmp/directory4").returns(true).at_least(1)
                File.expects(:exists?).with("/tmp/subdir/directory6").returns(true).at_least(1)

                Process.expects(:spawn).with("rsync -vrht --delete-before --delete-excluded /tmp/directory4 /tmp/my-backup/nodename_2").returns(@pid)
                Process.expects(:wait).with(@pid)

                Process.expects(:spawn).with("rsync -vrht --delete-before --delete-excluded /tmp/subdir/directory6 /tmp/my-backup/nodename_2").returns(@pid+1)
                Process.expects(:wait).with(@pid+1)

                @sut.updateFiles
            }
        }
    }
end
