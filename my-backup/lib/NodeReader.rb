#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class NodeReader
    def initializeBackupFromNodeFile backup, nodeFile
        if File.exists?(nodeFile)
            File.open(nodeFile, 'r') { |f|
                f.each_line { |line|
                    next if line.empty? or line == "\n"

                    if m = line.match(/^nodename:(.*)$/)
                        @nodename = m[1]
                    elsif m = line.match(/^backup: destination:(.*) source:(.*) (extra:\(.*\))\n?$/) || line.match(/^backup: destination:(.*) source:(.*)\n?$/)
                        backup.addSource(nodename, evaluate(m[1]), evaluate(m[2]))
                        if m[3] and extra = m[3].match(/^extra:\((.*)\)\n?$/)
                            tmpTag = extra[1]
                            while tmpTag and extra = tmpTag.match(/^(\w+):(.+?)( \w+:.+|\n|$)/)
                                method = 'addSource' + evaluate(extra[1]).capitalize
                                backup.send(method.to_sym, nodename, evaluate(m[1]), evaluate(m[2]), evaluate(extra[2]))

                                tmpTag = extra[3].strip
                            end
                        end
                    elsif m = line.match(/^noArchive: destination:(.*)\n?$/)
                        backup.addNoArchiveDetination(evaluate(m[1]))
                    else
                        raise "Wrong syntax in command '#{line.chomp}'"
                    end
                }
            }
        end
    ensure
        @nodename = nil
    end

private
    def nodename
        raise "Nodename not set before using other keywords" unless @nodename
        @nodename
    end

    def evaluate text
        while m = text.match(/#\{ENV\['(\w+)'\]\}/)
            text.gsub!(m[0], ENV[m[1]])
        end

        text
    end
end
