#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'NodeReader'
require_relative 'System'
require 'Colorize'
require 'shellwords'
require 'fileutils'
require 'digest'
require 'set'

class Backup
    class Source
        attr_reader :name, :excludes, :includes, :options
        attr_accessor :mode

        def initialize name
            @name = name
            @excludes = Set.new
            @includes = Set.new
            @options = Set.new
        end
    end

    def self.fromNodeFile nodeFile
        backup = Backup.new
        reader = NodeReader.new
        reader.initializeBackupFromNodeFile(backup, nodeFile)
        backup
    end

    def storeOldBackup
        each_dest(this_nodename) { |destination|
            backupPath = "#{destination}/#{this_nodename}"
            if archiveDestination?(destination) and File.exists?(backupPath)
                if File.exists? "#{backupPath}.tar"
                    do_between_spawn("cd #{Shellwords.escape(destination)} && tar -uvf #{Shellwords.escape(this_nodename)}.tar #{Shellwords.escape(this_nodename)}", :verbose => true)
                else
                    do_between_spawn("cd #{Shellwords.escape(destination)} && tar -cvf #{Shellwords.escape(this_nodename)}.tar #{Shellwords.escape(this_nodename)}", :verbose => true)
                end
            end
        }
    end

    def updateFiles
        each_source(this_nodename) { |destination, source|
            excludes = source.excludes.collect { |ex|
                "--exclude #{Shellwords.escape(ex)}"
            }.join(' ')

            includes = source.includes.collect { |inc|
                "--include #{Shellwords.escape(inc)}"
            }.join(' ')

            options = source.options.collect { |opt|
                Shellwords.escape(opt)
            }.join(' ')

            case source.mode
            when 'tar'
                begin
                    target = "#{destination}/#{this_nodename}/#{File.basename(source.name)}.tar.xz"
                    tmpSourceTar = "/tmp/#{File.basename(source.name)}.tar"
                    tmpSource = "/tmp/#{File.basename(source.name)}.tar.xz"
                    sum = nil

                    command = "tar #{excludes} -cf #{Shellwords.escape(tmpSourceTar)} #{Shellwords.escape(source.name)}"
                    do_between_spawn(command)
                    do_between_spawn("xz #{options} -z #{Shellwords.escape(tmpSourceTar)}") {
                        sum = Digest::MD5.file(target).hexdigest if File.exists?(target)
                    }

                    if sum != Digest::MD5.file(tmpSource).hexdigest
                        FileUtils.mv(tmpSource, target, :verbose => true)
                    end
                ensure
                    FileUtils.rm(tmpSourceTar) if File.exists?(tmpSourceTar)
                    FileUtils.rm(tmpSource) if File.exists?(tmpSource)
                end
            when 'dd'
                begin
                    target = "#{destination}/#{this_nodename}/#{File.basename(source.name)}"

                    unless File.exists?(target)
                        do_between_spawn("dd if=#{Shellwords.escape(source.name)} of=#{Shellwords.escape(target)} bs=1G conv=fsync")
                    end
                rescue Interrupt
                    FileUtils.rm(target) if File.exists?(target)

                    raise
                end
            when 'rsync', nil
                command = "rsync -vrht --delete-before --delete-excluded #{options} #{includes} #{excludes} #{Shellwords.escape(source.name)} #{Shellwords.escape(destination)}/#{Shellwords.escape(this_nodename)}"
                do_between_spawn(command, :verbose => true)
            else
                raise 'Unknown source mode'
            end
        }
    end

    def addSource nodename, destination, source
        if access(nodename, destination).to_a.index { |item| File.basename(item.name) == File.basename(source) }
            raise 'Source basename is duplicated within this destination'
        end

        access(nodename, destination).add(Source.new(source))
    end

    def addSourceExclude nodename, destination, source, exclude
        access(nodename, destination, source).excludes.add(exclude)
    end

    def addSourceInclude nodename, destination, source, inc
        access(nodename, destination, source).includes.add(inc)
    end

    def addSourceOption nodename, destination, source, opt
        access(nodename, destination, source).options.add(opt)
    end

    def addSourceMode nodename, destination, source, mode
        access(nodename, destination, source).mode = mode
    end

    def addNoArchiveDetination value
        accessNoArchive(:destination).add(value)
    end

    def archiveDestination? value
        not accessNoArchive(:destination).include?(value)
    end

    def destinations nodename
        access(nodename).keys
    end

    def sources nodename, destination
        access(nodename, destination).to_a
    end

private
    def each_dest nodename, &block
        destinations(nodename).each { |destination|
            block.call(destination) if File.exists?(destination)
        }
    end

    def each_source nodename, &block
        each_dest(nodename) { |destination|
            sources(nodename, destination).each { |source|
                block.call(destination, source) if File.exists?(source.name)
            }
        }
    end

    def do_between_spawn command, opts = Hash.new
        command = command.squeeze(' ')

        puts "spawn '#{command}'".faint

        pid = (opts[:verbose] ? Process.spawn(command) : Process.spawn(command, [:out, :err] => '/dev/null'))

        yield if block_given?

        Process.wait(pid)
    rescue Interrupt
        if pid
            Process.kill('TERM', pid)
            Process.wait(pid)
        end

        raise
    end

    def access nodename, destination = nil, source = nil
        r = @value ||= Hash.new
        r = @value[nodename] ||= Hash.new if nodename
        r = @value[nodename][destination] ||= Set.new if destination
        r = @value[nodename][destination].select{ |x| x.name == source }.first if source
        r
    end

    def accessNoArchive symbol
        r = @noArchive ||= Hash.new
        r = @noArchive[symbol] ||= Set.new if symbol
        r
    end

    def this_nodename
      System.nodename
    end
end
