Gem::Specification.new { |s|
  s.name        = 'my-backup'
  s.version     = '0.0.4'
  s.date        = DateTime.now.strftime('%Y-%m-%d')
  s.summary     = "#{s.name} library"
  s.description = "Rsyncs selected directories to specified backup device"
  s.authors     = ["chaos0x8"]
  s.files       = Dir['lib/*.rb', 'bin/*.rb']
  s.executables = Dir['bin/*.rb'].collect { |x| File.basename(x) }
  s.add_runtime_dependency 'include', '~> 0.4.0'
}
