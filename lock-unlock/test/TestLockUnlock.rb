#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'shoulda'
require 'mocha'

autoload :SecureRandom, 'securerandom'
autoload :FileUtils, 'fileutils'

class TestLockUnlock < Test::Unit::TestCase
  context('TestLockUnlock') {
    setup {
      @src = File.expand_path("#{File.dirname(__FILE__)}/../lib/LockUnlock.rb")
      @fn = "/tmp/#{SecureRandom.hex}_LockUnlock.rb"
    }

    teardown {
      FileUtils.rm @fn if File.exist? @fn
    }

    context('with copy') {
      setup {
        FileUtils.cp @src, @fn
        load @fn
      }

      should('create file path') {
        assert_equal('/tmp/fn', LockUnlock.accurateFileName(@fn, 'fn'))
      }
    }

    context('with symlink') {
      setup {
        FileUtils.ln_s "#{File.dirname(__FILE__)}/../lib/LockUnlock.rb", @fn
        load @fn
      }

      should('create file path') {
        assert_equal("#{File.dirname(@src)}/fn", LockUnlock.accurateFileName(@fn, 'fn'))
      }
    }
  }
end
