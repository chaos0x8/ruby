Gem::Specification.new { |s|
  s.name        = 'lock-unlock'
  s.version     = '0.0.5'
  s.date        = File.mtime(__FILE__).strftime('%Y-%m-%d')
  s.summary     = "#{s.name} library"
  s.description = "Subscriber for lock and unlock dbus event and exeutes scrips located in /home/$USER/lock.d"
  s.authors     = ["chaos0x8"]
  s.files       = Dir['lib/*.rb', 'bin/*.rb']
  s.executables = Dir['bin/*.rb'].collect { |x| File.basename(x) }
  s.add_runtime_dependency 'include', '~> 0.4.0'
}
