#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative '../lib/LockUnlockScriptRunner'
require_relative '../lib/LockUnlock'

begin
    Signal.trap "TERM" do
        ScriptRunner.runScripts "Terminate"
        abort "#{__FILE__}: Terminated!"
    end

    ScriptRunner.runScripts "Init"
    LISTENER = LockUnlock.accurateFileName(__FILE__, "lock-unlock-listener.rb")
    RUNNER = LockUnlock.accurateFileName(__FILE__, "lock-unlock-script-runner.rb")
    system "#{LISTENER} | #{RUNNER}"
    ScriptRunner.runScripts "Terminate"
rescue => e
    ScriptRunner.runScripts "Error"
    abort "#{__FILE__}: #{e}"
end
