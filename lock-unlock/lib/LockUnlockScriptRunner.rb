#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

autoload :FileUtils, 'fileutils'

module ScriptRunner
  class BusActivationFailed < RuntimeError
    def initialize
      super "Bus activation failed!"
    end
  end

  def checkActivation
    activationText = $stdin.readline
    raise BusActivationFailed.new if activationText.match(/member=NameAcquired/).nil?
    $stdin.readline
  end

  def runScripts lockState
    scripts = Dir["/home/#{ENV['USER']}/lock.d/*"].select { |fn| ! File.directory?(fn) and File.executable?(fn) }
    threads = scripts.collect { |script|
      Thread.new {
        outFn = "/tmp/#{File.basename(script)}.lock-unlock.log"
        pid, st = Process.wait2(Process.spawn(script, lockState, out: outFn, err: [:child, :out]))
        FileUtils.rm(outFn) if File.exist?(outFn) and st.exitstatus == 0
      }
    }

    threads.each { |t|
      t.kill unless t.join(60.0)
    }
  end

  def listenForChange
    textMember = $stdin.readline
    textState = $stdin.readline

    unless textMember.match(/ScreenSaver;\s+member=ActiveChanged/).nil?
      stateMatch = textState.match(/boolean (true|false)/)
      unless stateMatch.nil?
        case stateMatch[1]
        when "true"
          return "Lock"
        when "false"
          return "Unlock"
        end
      end
    end

    nil
  end

  module_function :checkActivation, :runScripts, :listenForChange
end
