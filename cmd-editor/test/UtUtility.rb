#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'securerandom'

module UtUtility
  def setFileContent content
    content = content.join("\n") if content.kind_of? Array

    @fn ||= "/tmp/#{SecureRandom.hex}_testCmdEditorFile.txt"
    File.open(@fn, 'w') { |f|
      f.write content
    }
  end

  def assert_file_content expectedContent
    expectedContent = expectedContent.join("\n") if expectedContent.kind_of? Array
    assert_equal(expectedContent, File.open(@fn, 'r') { |f| f.read })
  end

  def assert_file_content_match expectedContent
    expectedContent = expectedContent.join("\n") if expectedContent.kind_of? Array
    assert_match(expectedContent, File.open(@fn, 'r') { |f| f.read })
  end

  def self.defTeardown
    proc {
      teardown {
        if @fn and File.exist?(@fn)
          FileUtils.rm @fn
        end
      }
    }
  end
end
