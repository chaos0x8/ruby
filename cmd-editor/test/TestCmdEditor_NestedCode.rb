#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016 - 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative '../lib/CmdEditor'

require_relative 'UtUtility'

class TestCmdEditor < Test::Unit::TestCase
  include UtUtility

  context('with nested code') {
    merge_block(&UtUtility.defTeardown)

    setup {
      setFileContent([
        'class Foo',
        '{',
        'public:',
        '  void bar()',
        '  {',
        '    auto a = std::vector<int>({1,2,3});',
        '    if (std::find_if(a.begin(), a.end(), [](auto x) { return x == 2; } != a.end()) {',
        '      baz();',
        '    }',
        '  }',
        '',
        '  void baz()',
        '  {',
        '',
        '  }',
        '}; // class Foo',
        '',
        'int main()',
        '{',
        '  return 0;',
        '} // int main()'
      ])
    }

    should('find matching bracket/1') {
      result = CmdEditor.read(@fn) { |e|
        e[e.find(matching: '{')]
      }

      assert_equal('}; // class Foo', result)
    }

    should('find matching bracket/2') {
      result = CmdEditor.read(@fn) { |e|
        e[e.find({ after_including: { first: 'std::vector' }}, matching: '{')]
      }

      assert_match('std::vector<int>({1,2,3})', result)
    }

    should('find matching bracket/3') {
      result = CmdEditor.read(@fn) { |e|
        e[e.find({ after: { first: /\bif\b/ }}, matching: '(')]
      }

      assert_match('baz();', result)
    }

    should('find matching bracket/4') {
      result = CmdEditor.read(@fn) { |e|
        e[e.find({ after: { matching: '{' }}, matching: '{')]
      }

      assert_equal('} // int main()', result)
    }
  }
end
