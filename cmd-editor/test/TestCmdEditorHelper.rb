#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/CmdEditorHelper'

class TestCmdEditorHelper < Test::Unit::TestCase
  include CmdEditorHelper

  context('CmdEditorHelper') {
    should('return indent value of line') {
      assert_equal(0, indent('hello'))
      assert_equal(2, indent('  hello'))
      assert_equal(4, indent('    hello'))
    }

    should('return indent value of line with tabs') {
      assert_equal(2, indent("\thello", tabwidth: 2))
      assert_equal(6, indent(" \t hello", tabwidth: 4))
      assert_equal(6, indent(" \t hello"))
    }

    should('set indent') {
      assert_equal('  hello', set_indent(" \t hello", 2))
      assert_equal('    hello', set_indent('hello', 4))
      assert_equal('hello', set_indent('  hello', 0))
    }
  }
end

