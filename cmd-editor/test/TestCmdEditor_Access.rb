#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative '../lib/CmdEditor'

require_relative 'UtUtility'

class TestCmdEditor < Test::Unit::TestCase
  include UtUtility

  context('TestCmdEditor.[]') {
    merge_block(&UtUtility.defTeardown)

    setup {
      setFileContent(['line1', 'line2', 'line3'])

      @yielded = Array.new
    }

    should('access single line') {
      CmdEditor.read(@fn) { |e|
        assert_equal('line2', e[2])
      }
    }

    should('access few lines') {
      CmdEditor.read(@fn) { |e|
        assert_equal(['line1', 'line3'], e[[1,3]])
      }
    }

    should('raise argument error when passing \'0\' as index when accessing line') {
      CmdEditor.read(@fn) { |e|
        assert_raise(ArgumentError) {
          e[0]
        }
      }
    }

    should('yield every line') {
      CmdEditor.read(@fn) { |e|
        e.each { |line| @yielded << line }
      }

      assert_equal(['line1', 'line2', 'line3'], @yielded)
    }

    should('yield single line') {
      CmdEditor.read(@fn) { |e|
        e.each(3) { |line| @yielded << line }
      }

      assert_equal(['line3'], @yielded)
    }

    should('yield few lines') {
      CmdEditor.read(@fn) { |e|
        e.each(1,3) { |line| @yielded << line }
      }

      assert_equal(['line1', 'line3'], @yielded)
    }

    should('raise argument error when passing \'0\' as one of indexes when accessing line') {
      CmdEditor.read(@fn) { |e|
        assert_raise(ArgumentError) {
          e.each(1,0,3)
        }
      }
    }
  }
end

