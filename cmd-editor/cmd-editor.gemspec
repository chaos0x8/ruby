Gem::Specification.new { |s|
  s.name        = 'cmd-editor'
  s.version     = '0.1.0'
  s.date        = File.mtime(__FILE__).strftime('%Y-%m-%d')
  s.summary     = "#{s.name}"
  s.description = "application/library which allows file edition with commands"
  s.authors     = ["chaos0x8"]
  s.files       = Dir['lib/**/*.rb', 'bin/*.rb']
  s.executables = Dir['bin/*.rb'].collect { |x| File.basename(x) }
  s.add_runtime_dependency 'include', '~> 0.2.7'
  s.add_runtime_dependency 'test-support', '~> 0.0.1'
}
