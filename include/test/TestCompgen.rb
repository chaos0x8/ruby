#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'TestSupport'

require_relative '../lib/Compgen'

class TestCompgen < Test::Unit::TestCase
  include Compgen

  def values= *args
    @values = [ *args ]
  end

  def patern= val
    @patern = val
  end

  context('TestCompgen') {
    with(:values= => []) {
      context('.commonStr') {
        should('return empty string') {
          assert_equal('', commonStr(*@values))
        }
      }
    }

    with(:values= => [ 'ABC_1010', 'ABC_1020', 'ABC_2000' ]) {
      context('.commonStr') {
        should('return longest common part') {
          assert_equal('ABC_', commonStr(*@values))
        }
      }

      context('compgen') {
        should('return matching patern') {
          assert_equal([], compgen(values: @values, patern: 'B'))
          assert_equal(['ABC_1010', 'ABC_1020', 'ABC_2000'], compgen(values: @values, patern: 'A'))
          assert_equal(['ABC_1010', 'ABC_1020'], compgen(values: @values, patern: 'ABC_1'))
        }
      }

      context('compgenEx') {
        should('return matching patern') {
          assert_equal([], compgenEx(values: @values, patern: 'BCD'))
          assert_equal(['ABC_1010', 'ABC_1020', 'ABC_2000'], compgenEx(values: @values, patern: 'B'))
          assert_equal(['ABC_1010', 'ABC_1020'], compgenEx(values: @values, patern: 'BC_1'))
        }
      }
    }
  }
end
