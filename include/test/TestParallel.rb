#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016 - 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/Parallel'

class TestParallel < Test::Unit::TestCase
  class Data
    attr_reader :input, :output, :collected

    def initialize input, output, collected
      @input = input
      @output = output
      @collected = collected
    end
  end

  context('TestParallel') {
    setup {
      @data = (1..20).collect { |i|
        Data.new(i, i+100, i+1000)
      }

      @mock = mock()
    }

    setup {
      @data.each { |x|
        @mock.expects(:process).with(x.input).returns(x.output).at_least(0)
        @mock.expects(:collect).with(x.output).returns(x.collected).at_least(0)
      }
    }

    teardown {
      @sut.join if @sut
    }

    should('process data') {
      @sut = Parallel.new(
        input: @data.collect { |x| x.input },
        process_proc: proc { |i|
          @mock.process( i )
        }).join

      assert_equal(@data.collect { |x| x.output }, @sut.result.sort)
    }

    should('process data with collect') {
      @sut = Parallel.new(
        input: @data.collect { |x| x.input },
        process_proc: proc { |i|
          @mock.process( i )
        },
        collect_proc: proc { |o|
          @mock.collect( o )
        }).join

      assert_equal(@data.collect { |x| x.collected }, @sut.result.sort)
    }

    should('process data with join') {
      @mock.expects(:join).with { |x| x.sort == @data.collect { |y| y.output } }.returns(70)

      @sut = Parallel.new(
        input: @data.collect { |x| x.input },
        process_proc: proc { |i|
          @mock.process( i )
        },
        join_proc: proc { |r|
          @mock.join( r )
        }).join

      assert_equal(70, @sut.result)
    }

    should('process data with collect and join') {
      @mock.expects(:join).with { |x| x.sort == @data.collect { |y| y.collected } }.returns(42)

      @sut = Parallel.new(
        input: @data.collect { |x| x.input },
        process_proc: proc { |i|
          @mock.process( i )
        },
        collect_proc: proc { |o|
          @mock.collect( o )
        },
        join_proc: proc { |r|
          @mock.join( r )
        }).join

      assert_equal(42, @sut.result)
    }

    should('use custom nproc') {
      @sut = Parallel.new(
        nproc: 1000,
        input: @data.collect { |x| x.input },
        process_proc: proc { |i|
          @mock.process( i )
        },
        collect_proc: proc { |o|
          @mock.collect( o )
        })
    }
  }
end
