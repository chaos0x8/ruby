#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/Retry'

class TestRetry < Test::Unit::TestCase
  include Retry

  context('TestRetry') {
    setup {
      @count = 0
    }

    should('execute once when everything is ok') {
      try(10) {
        @count += 1
      }

      assert_equal(1, @count)
    }

    should('execute until its ok') {
      try(10) {
        @count += 1

        raise 'xxx' if @count != 5
      }

      assert_equal(5, @count)
    }

    should('raise when last attempt also fails') {
      assert_raise(RuntimeError) {
        try(10) {
          @count += 1

          raise 'xxx'
        }
      }

      assert_equal(10, @count)
    }

    context('name error') {
      setup {
        @fun = proc {
          @count += 1

          unknown
        }
      }

      should('raise at first attempt when exception is not RuntimeError') {
        assert_raise(NameError) {
          try(10, &@fun)
        }

        assert_equal(1, @count)
      }

      should('properly retry in case of custom exception') {
        assert_raise(NameError) {
          try(5, exception: NameError, &@fun)
        }

        assert_equal(5, @count)
      }
    }
  }
end

