#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016 - 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'TestSupport'

require_relative '../../lib/settingfile/Storage'

class TestSettingFileStorage < Test::Unit::TestCase
  def fileData= data
      @file = mock()

      File.expects(:exist?).with(@fileName).returns(true).at_least(0)
      File.expects(:open).with(@fileName, 'r').returns(data).yields(@file).at_least(0)

      @file.expects(:read).returns(data).at_least(0)
  end

  context('Test SettingFile::Storage') {
    setup {
      @fileName = 'fn'

      File.expects(:open).at_most(0)
    }

    should('return nil when file doesn\'t exist?') {
      File.expects(:exist?).with(@fileName).returns(false).at_least(0)

      val = SettingFile::Storage.read(@fileName, :value) { |a0|
        assert_equal(nil, a0)
      }

      assert_equal(nil, val)
    }

    with(:fileData= => '') {
      should('do nothing when write with no keys') {
        SettingFile::Storage.write(@fileName)
      }

      context('with write attempt') {
        setup {
          File.expects(:open).with(@fileName, 'w').yields(@file)
        }

        should('write file') {
          @file.expects(:write).with("k1=v1\n")
          @file.expects(:write).with("k2=v2\n")

          SettingFile::Storage.write(@fileName, k1: 'v1', k2: 'v2')
        }
      }
    }

    with(:fileData= => '#_key_=value') {
      should('raise when key is not converted to symbol') {
        assert_raise(SettingFile::Storage::InvalidKeyError) {
          assert_equal(nil, SettingFile::Storage.read(@fileName, '#_key_'))
        }
      }
    }

    with(:fileData= => '_key_=_value_=stuff') {
      should('read from config file when values contains equal sign') {
        val = SettingFile::Storage.read(@fileName, :_key_) { |a0|
          assert_equal('_value_=stuff', a0)
        }

        assert_equal('_value_=stuff', val)
      }
    }

    with(:fileData= => '_key_=_value_') {
      should('return nil when trying to read zero keys') {
        assert_equal(nil, SettingFile::Storage.read(@fileName))
      }

      should('read from config file') {
        assert_equal('_value_', SettingFile::Storage.read(@fileName, :_key_))
      }

      should('read multiple keys from config file') {
        assert_equal([ '_value_', '_value_' ], SettingFile::Storage.read(@fileName, :_key_, :_key_))
      }

      context('write case') {
        setup {
          File.expects(:open).with(@fileName, 'w').yields(@file)
        }

        should('write single copy of variable') {
          @file.expects(:write).with("_key_=_value_\n")
          @file.expects(:write).with("x1=4=4\n")
          @file.expects(:write).with("x2=42\n")

          SettingFile::Storage.write(@fileName, x1: '4=4', x2: 15, 'x2' => 42)
        }

        should('delete existing value') {
          SettingFile::Storage.write(@fileName, _key_: nil)
        }

        should('change existing value') {
          @file.expects(:write).with("_key_=_new_value_\n")

          SettingFile::Storage.write(@fileName, _key_: '_new_value_')
        }
      }

      context('invalid key') {
        should('raise when reading symbol is invalid') {
          sut = SettingFile::Storage::Proxy.new(@fileName)

          assert_raise(SettingFile::Storage::InvalidKeyError) {
            sut['#key']
          }
        }

        should('raise when writting symbol is invalid/1') {
          assert_raise(SettingFile::Storage::InvalidKeyError) {
            SettingFile::Storage.write(@fileName, '#key': 'val')
          }
        }

        should('raise when writting symbol is invalid/2') {
          sut = SettingFile::Storage::Proxy.new(@fileName)

          assert_raise(SettingFile::Storage::InvalidKeyError) {
            sut['#key'] = 'val'
          }
        }
      }
    }
  }
end
