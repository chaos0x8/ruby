#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../../lib/file/Follow'

class TestFileFollow < Test::Unit::TestCase
    def self.shouldReturn opts
        Proc.new {
            opts.each { |key, val|
                should("return #{key}") {
                    assert_equal(val, File.send(key, @filename))
                }
            }
        }
    end

    context('TestFileFollow') {
        setup {
            @filename = '/dir/file'
            @linkname = '/linkdir/linkname'

            File.expects(:symlink?).at_most(0)
            File.expects(:readlink).at_most(0)
        }

        context('with regulard file') {
            setup {
                File.expects(:symlink?).with(@filename).returns(false).at_least(0)
            }

            merge_block(&shouldReturn(:targetName => '/dir/file', :targetDirname => '/dir'))
        }

        context('with symlink') {
            setup {
                File.expects(:symlink?).with(@filename).returns(true).at_least(0)
                File.expects(:readlink).with(@filename).returns(@linkname).at_least(0)
            }

            merge_block(&shouldReturn(:targetName => '/linkdir/linkname', :targetDirname => '/linkdir'))
        }
    }
end
