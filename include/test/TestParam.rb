#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'TestSupport'

require_relative '../lib/Param'
require_relative 'param/UtUtility'

class ParamTestSuite < Test::Unit::TestCase
    def assert_help expected, actual
      assert_equal(expected.size, actual.size)
      expected.each_index { |i|
        assert_match(expected[i], actual[i])
      }
    end

    def mandatoryArgsSut
      Param[
        Param::Arg::Value['--val'],
        Param::Mod::MandatoryArgs['--val']]
    end

    def reassignSut
      Param[
        Param::Arg::Value['--val1'],
        Param::Arg::Value['--val2'],
        Param::Mod::ReassignFreeArgs['--val1']]
    end

    def reassignListSut
      Param[
        Param::Arg::Set['--set'],
        Param::Mod::ReassignFreeArgs['--set']]
    end

    def multipleReassignSut
      Param[
        Param::Arg::Value['--val1'],
        Param::Arg::Value['--val2'],
        Param::Arg::Value['--val3'],
        Param::Mod::ReassignFreeArgs['--val1', '--val2'],
        Param::Mod::ReassignFreeArgs['--val3']]
    end

    def anyOfMandatoryArgsSut
      Param[
        Param::Arg::Value['--val1'],
        Param::Arg::Value['--val2'],
        Param::Mod::MandatoryArgs::AnyOf['--val1', '--val2']]
    end

    def anyOfMandatoryGroupArgsSut
      Param[
        Param::Arg::Value['--val1'],
        Param::Arg::Value['--val2'],
        Param::Arg::Value['--val3'],
        Param::Arg::Value['--val4'],
        Param::Mod::MandatoryArgs::AnyOf[
          ['--val1', '--val3'],
          Param::Mod::MandatoryArgs['--val2', '--val4', '--val3']]]
    end

    context('TestParam') {
        teardown {
            ARGV.clear
        }

        context('::Mod::ReassignFreeArgs') {
            with(:args => '--val2=42') {
                should('do nothing when reassign param not passed') {
                    sut = reassignSut

                    assert_equal(nil, sut['--val1'])
                    assert_equal('42', sut['--val2'])
                    assert_equal([], sut.freeArgs)
                }
            }

            with(:args => [ 'text1', 'anotherParam' ]) {
                should('reassign param') {
                    sut = reassignSut

                    assert_equal('text1', sut['--val1'])
                    assert_equal(nil, sut['--val2'])
                    assert_equal(['anotherParam'], sut.freeArgs)
                }
            }

            with(:args => ['arg1', 'arg2', 'arg3']) {
                should('reassign params named parametes') {
                    sut = multipleReassignSut

                    assert_equal('arg1', sut['--val1'])
                    assert_equal('arg2', sut['--val2'])
                    assert_equal('arg3', sut['--val3'])
                }

                should('reassign params to list') {
                    sut = reassignListSut

                    assert_equal(['arg1', 'arg2', 'arg3'], sut['--set'])
                }
            }
        }

        context('::Mod::AssumeFreeAsUnexpected') {
            with(:args => '--val1=arg1') {
                should('be ok when all pased arguments are defined') {
                    Param[Param::Arg::Value['--val1'],
                          Param::Mod::AssumeFreeAsUnexpected[]]
                }

                should('raise when passed arguments is assigned to freeArgs') {
                    assert_raise(Param::Ex::Unknown) {
                        Param[Param::Mod::AssumeFreeAsUnexpected[]]
                    }
                }
            }

            with(:args => 'arg1') {
                should('be ok when freeArg is reassigned') {
                    Param[Param::Arg::Value['--val1'],
                          Param::Mod::ReassignFreeArgs['--val1'],
                          Param::Mod::AssumeFreeAsUnexpected[]]
                }
            }
        }

        context('::Mod::MandatoryArgs') {
            should('raise when mandatory param is missing') {
                assert_raise(Param::Ex::MissingMandatory) {
                    mandatoryArgsSut
                }
            }

            with(:args => '--val=17') {
                should('be ok when mandatory arg is present') {
                    mandatoryArgsSut
                }

                should('raise when mandatory param is not a param') {
                    assert_raise(Param::Ex::MissingMandatory) {
                        Param[Param::Mod::MandatoryArgs['--val']]
                    }
                }
            }

            with(:args => '17') {
                should('be ok when mandatory arg is reassigned') {
                    Param[Param::Arg::Value['--val'],
                          Param::Mod::ReassignFreeArgs['--val'],
                          Param::Mod::MandatoryArgs['--val']]
                }
            }
        }

        context('::Mod::MandatoryArgs::AnyOf') {

            should('raise when all of alternative params are missing') {
                assert_raise(Param::Ex::MissingMandatory) {
                    anyOfMandatoryArgsSut
                }
            }

            [ '--val1=13', '--val2=17' ].each { |argument|
                with(:args => argument) {
                    should('be ok when one of mandatory params is present') {
                        anyOfMandatoryArgsSut
                    }
                }
            }


            [ ['--val1=1', '--val3=3'], ['--val2=2', '--val3=3', '--val4=4'] ].each { |arguments|
                with(:args => arguments) {
                    should('be ok when all group elements are present') {
                        anyOfMandatoryGroupArgsSut
                    }
                }
            }

            [ ['--val1=1' ], ['--val3=3', '--val4=4'] ].each { |arguments|
                with(:args => arguments) {
                    should('raise when any of alternative group is not complete') {
                        assert_raise(Param::Ex::MissingMandatory) {
                            anyOfMandatoryGroupArgsSut
                        }
                    }
                }
            }
        }

        should('raise when unknown param is requested') {
            sut = Param[]

            assert_raise(Param::Ex::Unknown) {
                sut['--unknown']
            }
        }

        context('.help') {
            setup {
                @args = [ Param::Arg::Option.new('--opt', description: 'comment1', default: true),
                          Param::Arg::Value.new('--val', description: 'comment2', default: 42),
                          Param::Arg::Set.new('--list', description: 'comment3', default: [ 10, 12 ]) ]
                @sut = Param.new(*@args)

                @expected = [ /^--opt - comment1$/,
                              /^\s+default: true$/,
                              /^--val - comment2$/,
                              /^\s+default: 42$/,
                              /^--list - comment3$/,
                              /^\s+default: \["10", "12"\]$/ ]
            }

            teardown {
                assert_help(@expected, @actual)
            }

            should('display descriptive help') {
                @actual = @sut.help.split("\n")
            }

            should('display descriptive help from static method') {
                @actual = Param._help(@args).split("\n")
            }
        }
    }
end
