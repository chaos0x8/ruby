#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'shoulda'
require 'mocha'

require_relative '../lib/Escape'

class TestEscape < Test::Unit::TestCase
  context('TestEscape') {
    context('.join') {
      should('join array') {
        assert_equal('1;2', Escape.join([1,2]))
      }

      should('join array using different separator') {
        assert_equal('1_2', Escape.join([1,2], '_'))
      }

      should('join array containing characters to escape') {
        assert_equal('x\\\\x;x\\;x', Escape.join(['x\\x', 'x;x']))
      }

      should('join array containing characters to escape using different separator') {
        assert_equal('1_x\\_x', Escape.join(['1', 'x_x'], '_'))
      }

      should('join empty array') {
        assert_equal(nil, Escape.join([]))
        assert_equal('', Escape.join([nil]))
      }

      should('join array with empty elements') {
        assert_equal(';', Escape.join([nil,nil]))
        assert_equal('1;', Escape.join([1,nil]))
        assert_equal('1;;2', Escape.join([1,nil,2]))
      }
    }

    context('.split') {
      should('split string') {
        assert_equal(['1','2'], Escape.split('1;2'))
      }

      should('split string using different separator') {
        assert_equal(['1','2'], Escape.split('1_2', '_'))
      }

      should('split string containing characters to escape') {
        assert_equal(['x\\x','x;x'], Escape.split('x\\\\x;x\\;x'))
      }

      should('split string containing characters to escape using different separator') {
        assert_equal(['1','x_x'], Escape.split('1_x\\_x', '_'))
      }

      should('split nil') {
        assert_equal([], Escape.split(nil))
      }

      should('split empty string') {
        assert_equal([''], Escape.split(''))
      }

      should('split string with empty elements') {
        assert_equal(['', ''], Escape.split(';'))
        assert_equal(['1', ''], Escape.split('1;'))
        assert_equal(['1', '', '2'], Escape.split('1;;2'))
      }
    }

    context('join&split') {
      [ [],
        [nil],
        [nil,1,nil],
        [1,2,3],
        ['\\',';'],
        ['\\;;',1]
      ].each_with_index { |arr, i|
        should("join and split/#{i}") {
          assert_equal(arr.collect(&:to_s), Escape.split(Escape.join(arr)))
        }
      }

      [ nil,
        '',
        ';1;',
        '1;2;3',
        '\\\\;\\;',
        '\\\\;;1',
      ].each_with_index { |txt, i|
        should("split and join/#{i}") {
          assert_equal(txt, Escape.join(Escape.split(txt)))
        }
      }
    }
  }
end
