#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016 - 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'shoulda'
require 'mocha/setup'

require_relative '../lib/SettingFile'

require 'securerandom'

class TestSettingFile < Test::Unit::TestCase
  def self.shouldAssignValue
    Proc.new {
      should('assign value') {
        assert_equal(42, @sut.foo = 42)

        assert_equal('42', @sut.foo)
      }
    }
  end

  def self.shouldReturnValuePresence
    Proc.new {
      should('return false before value is assigned') {
        assert_equal(false, @sut.foo?)
      }

      should('return true after value is assigned') {
        @sut.foo = 0

        assert_equal(true, @sut.foo?)
      }
    }
  end

  def self.shouldUseDefaultValue **opts
    Proc.new {
      should('use default value') {
        @sut.register(:bar, default: 'def', **opts)

        assert_equal('def', @sut.bar)
      }

      should('not override already present value') {
        @sut.register(:bar, default: 'def', **opts)

        @tmp = SettingFile.new(@fileName)
        @tmp.register(:bar, default: 'override', **opts)

        assert_equal('def', @tmp.bar)
        assert_equal('def', @sut.bar)
      }
    }
  end

  def self.shouldCollectValues **opts
    Proc.new {
      context('with collect') {
        setup {
          @sut.register(:bar, opts.merge(collect: proc { |val| val == 'true' ? 1 : 0 }))
        }

        should('not be present before assigned') {
          assert_equal(false, @sut.bar?)
        }

        should('be present after assigned') {
          @sut.bar = false

          assert_equal(true, @sut.bar?)
        }

        should('collect values') {
          @sut.bar = true
          assert_equal(1, @sut.bar)
        }
      }
    }
  end

  def self.shouldTransformValues **opts
    Proc.new {
      should('store values') {
        @sut.register(:bar, opts.merge(store: proc { |val| val.join(';') }))

        @sut.bar = [ 1, 2, 3 ]
        assert_equal('1;2;3', @sut.bar)
      }
    }
  end

  def self.shouldCreateFrozenOnlyClone **opts
    Proc.new {
      context('clone') {
        setup {
          @clone = @sut.frozen_clone
        }

        should('create read only clone') {
          assert(@clone.respond_to?(:foo))
          assert(@clone.respond_to?(:foo?))
          assert(! @clone.respond_to?(:foo=))
        }

        if opts[:required]
          should('raise on access') {
            assert_raise(MissingRequired) {
              @clone.foo
            }
          }
        else
          should('return nil on access') {
            assert_equal(nil, @clone.foo)
          }
        end
      }

      context('with value before clone') {
        setup {
          @sut.register :bar, opts.merge(collect: proc { |v| Integer(v) })
          @sut.foo, @sut.bar = 17, 42
          @clone = @sut.frozen_clone
        }

        should('keep values from frozen state') {
          @sut.bar = 15

          assert_equal(15, @sut.bar)
          assert_equal(42, @clone.bar)
        }

        should('be equal when not changed') {
          assert(@clone == @sut)
          assert_equal(false, @clone != @sut)
        }

        should('not be equal when changed') {
          @sut.bar = 10

          assert(@clone != @sut)
          assert_equal(false, @clone == @sut)
        }
      }
    }
  end

  context('TestSettingFile') {
    setup {
      @fileName = "/tmp/#{File.basename(__FILE__)}_#{SecureRandom.hex}.rc"
      @sut = SettingFile.new(@fileName)
    }

    teardown {
      FileUtils.rm @fileName if File.exist? @fileName
    }

    should('access fileName') {
      assert_equal(@fileName, @sut.fileName)
    }

    should('raise no method error when not registered') {
      assert_raise(NoMethodError) {
        @sut.foo
      }
    }

    context('with foo') {
      setup {
        @sut.register :foo
      }

      should('register parameter in specific instance') {
        assert_equal(nil, @sut.foo)

        assert_raise(NoMethodError) {
          SettingFile.new(@fileName).foo
        }
      }

      merge_block(&shouldAssignValue)
      merge_block(&shouldReturnValuePresence)
      merge_block(&shouldUseDefaultValue)
      merge_block(&shouldCollectValues)
      merge_block(&shouldTransformValues)
      merge_block(&shouldCreateFrozenOnlyClone)
    }

    context('with required foo') {
      setup {
        @sut.register :foo, required: true
      }

      should('raise when required parameter is not present') {
        assert_raise(SettingFile::MissingRequired) {
          @sut.foo
        }
      }

      should('raise when trying to assign nil to required parameter') {
        @sut.foo = 'hello'

        assert_raise(SettingFile::MissingRequired) {
          @sut.foo = nil
        }

        assert_equal('hello', @sut.foo)
      }

      should('raise when missing before collect') {
        @sut.register(:bar, required: true, collect: proc { |val| val == 'true' ? true : false })

        assert_raise(SettingFile::MissingRequired) {
          @sut.bar
        }
      }

      should('raise when missing before store') {
        @sut.register(:bar, required: true, store: proc { |val| val.join(';') })

        assert_raise(SettingFile::MissingRequired) {
          @sut.bar = nil
        }
      }

      merge_block(&shouldAssignValue)
      merge_block(&shouldReturnValuePresence)
      merge_block(&shouldUseDefaultValue(required: true))
      merge_block(&shouldCollectValues(required: true))
      merge_block(&shouldTransformValues(required: true))
      merge_block(&shouldCreateFrozenOnlyClone)
    }

    context('TestSettingFile.details') {
      [ { name: 'n1', default: 'def', expected: "name: n1, default: def\n  value: def" },
        { name: 'n2', default: 'false', expected: "name: n2, default: false\n  value: false" },
        { name: 'n3', required: true, expected: "name: n3, +required\n  value: #{'<nil>'.faint}" },
        { name: 'n4', required: false, collect: proc { |x| nil }, expected: "name: n4, +collect\n  value: #{'<nil>'.faint}" },
        { name: 'n5', store: proc { |x| nil }, expected: "name: n5, +store\n  value: #{'<nil>'.faint}" } ].each_with_index { |opts, i|

        should("show detail/#{i}") {
          name = opts[:name]
          expected = opts[:expected]

          opts.delete(:name)
          opts.delete(:expected)

          @sut.register(name, opts)

          assert_equal(expected, @sut.details)
        }
      }

      should('show multiple details') {
        @sut.register('n1', required: true)
        @sut.register('n2', default: false)

        expected = "name: n1, +required\n" +
                   "  value: #{'<nil>'.faint}\n" +
                   "name: n2, default: false\n" +
                   "  value: false"

        assert_equal(expected, @sut.details)
      }
    }
  }
end
