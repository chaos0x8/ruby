#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../../lib/imap/Operation'
require_relative '../../lib/imap/Encoding'

class TestImapOperation < Test::Unit::TestCase
  include Imap::Operation
  include Imap::Encoding

  context('Test Imap::Operation') {
    setup {
      @imap = mock
      @seq = sequence('seq')
    }

    context('::Move') {
      setup {
        @iter = 0
      }

      should('move to new directories') {
        @imap.expects(:select).in_sequence(@seq).with(e_utf7 'box')
        @imap.expects(:list).in_sequence(@seq).with('main/', 'sub1').returns(false)
        @imap.expects(:create).in_sequence(@seq).with('main/sub1')
        @imap.expects(:copy).in_sequence(@seq).with([7,1], 'main/sub1')
        @imap.expects(:store).in_sequence(@seq).with([7,1], '+FLAGS', [:Deleted])
        @imap.expects(:close).in_sequence(@seq)

        @imap.expects(:select).in_sequence(@seq).with(e_utf7 'box')
        @imap.expects(:list).in_sequence(@seq).with('main/', 'sub2').returns(true)
        @imap.expects(:copy).in_sequence(@seq).with([14], 'main/sub2')
        @imap.expects(:store).in_sequence(@seq).with([14], '+FLAGS', [:Deleted])
        @imap.expects(:close).in_sequence(@seq)

        fakeMoveData = [
          { [7,1] => 'main/sub1', 14 => 'main/sub2' },
          { nil => 'main/sub3', 14 => 'main/sub2' } ]

        Move.do(@imap, 'box') { |toMove|
          toMove << fakeMoveData.first
          fakeMoveData = fakeMoveData.drop(1)
          @iter += 1
        }

        assert_equal(2, @iter)
      }

      [
        nil,
        { [] => 'dir1' },
        { [nil] => 'dir1' }
      ].each_with_index { |data, idx|
        should("do nothing with nils/#{idx}") {
          @imap.expects(:select).in_sequence(@seq).with(e_utf7 'box')
          @imap.expects(:close).in_sequence(@seq)

          Move.do(@imap, 'box') { |toMove|
            toMove << data
            @iter += 1
          }

          assert_equal(1, @iter)
        }
      }
    }

    { Delete => [:Deleted], See => [:Seen] }.each { |cls, flags|
      context("::#{cls}") {
        should("store #{flags} flags for messages") {
          @imap.expects(:select).in_sequence(@seq).with(e_utf7 'box')
          @imap.expects(:store).in_sequence(@seq).with([3,10,7], '+FLAGS', flags)
          @imap.expects(:close).in_sequence(@seq)

          cls.do(@imap, 'box') { |toDel|
            toDel << [ 3, 10 ]
            toDel <<  3 << 7
          }
        } if cls == Delete

        should('do nothing when toDel list stays empty') {
          @imap.expects(:select).in_sequence(@seq).with(e_utf7 'box')
          @imap.expects(:close).in_sequence(@seq)

          cls.do(@imap, 'box') { |toDel|
            nil
          }
        }

        should('do nothing when toDel list contains only nils') {
          @imap.expects(:select).in_sequence(@seq).with(e_utf7 'box')
          @imap.expects(:close).in_sequence(@seq)

          cls.do(@imap, 'box') { |toDel|
            toDel << nil
          }
        }
      }
    }

    context('::Examine') {
      should('examine mailbox') {
        @imap.expects(:examine).in_sequence(@seq).with('box')
        @imap.expects(:close).in_sequence(@seq)

        Imap::Operation::Examine.do(@imap, 'box') {
          nil
        }
      }
    }
  }
end


