#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../../lib/imap/Date'

class TestImapDate < Test::Unit::TestCase
  include Imap::Date

  IMAP_FORMAT = /^\d{2}-\w{3}-\d{4}$/

  context('Test Imap::Date') {
    should('format DateTime') {
      assert_match(IMAP_FORMAT, DateTime.now.imapFormat)
    }

    should('return current date') {
      assert_equal(DateTime.now.imapFormat, 0.daysAgo_s)
    }

    should('return previous date') {
      assert(DateTime.now > DateTime.parse(1.daysAgo_s))
    }

    should('return date in imap format') {
      assert_match(IMAP_FORMAT, 2.daysAgo_s)
    }
  }
end

