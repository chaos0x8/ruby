#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../../lib/imap/Utility'

require 'base64'

class TestImapUtility < Test::Unit::TestCase
  include Imap::Utility

  def self.withAddrMock
    proc {
      setup {
        @addr = mock
        @addr.expects(:mailbox).returns('xxx').at_least(0)
        @addr.expects(:host).returns('yyy.com').at_least(0)
      }
    }
  end

  context('Test Imap::Utility') {
    setup {
      @imap = mock
      @seq = sequence('seq')
    }

    context('.manageImap') {
      should('login to imap server') {
        server = 'srv'
        port = 400
        user = 'usr'
        password = 'pass'
        ssl = true

        Net::IMAP.expects(:new).in_sequence(@seq).with(server, port: port, ssl: ssl).returns(@imap)
        @imap.expects(:login).in_sequence(@seq).with(user, password)
        @imap.expects(:logout).in_sequence(@seq)
        @imap.expects(:disconnect).in_sequence(@seq)

        actual = nil
        res = manageImap(server, port: port, ssl: ssl, user: user, password: password) { |obj|
          actual = obj
          42
        }

        assert_equal(@imap, actual)
        assert_equal(42, res)
      }
    }

    context('.addresses') {
      merge_block(&withAddrMock)

      should('return empty array in case of nil') {
        assert_equal([], addresses(nil))
      }

      should('convert to simple mail addresses') {
        assert_equal(['xxx@yyy.com'], addresses([@addr]))
      }
    }

    context('.hosts') {
      merge_block(&withAddrMock)

      should('convert to simple mail addresses') {
        assert_equal(['yyy.com'], hosts([@addr, @addr]))
      }
    }

    context('.mkdir') {
      should('make dir if not exist') {
        @imap.expects(:list).in_sequence(@seq).with('box/', 'dir').returns(false).at_least(0)
        @imap.expects(:create).with('box/dir')

        mkdir(@imap, 'box/dir')
      }

      should('do nothing when directory already exist') {
        @imap.expects(:list).in_sequence(@seq).with('box/', 'dir').returns(true).at_least(0)

        mkdir(@imap, 'box/dir')
      }
    }

    context('.rmdir') {
      should('remove dir if exist') {
        @imap.expects(:list).in_sequence(@seq).with('box/', 'dir').returns(true).at_least(0)
        @imap.expects(:delete).with('box/dir')

        rmdir(@imap, 'box/dir')
      }

      should('do nothing when directory doesn\'t exist') {
        @imap.expects(:list).in_sequence(@seq).with('box/', 'dir').returns(false).at_least(0)

        rmdir(@imap, 'box/dir')
      }
    }
  }
end

