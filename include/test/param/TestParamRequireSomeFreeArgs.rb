# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'TestSupport'
require_relative 'UtUtility'

require_relative '../../lib/Param'

class TestParamRequireSomeFreeArgs < Test::Unit::TestCase
  context('TestParamRequireSomeFreeArgs') {
    setup {
      (class << self; self; end).class_eval {
        define_method(:sut) {
          @sut ||= Param[
            Param::Arg::Value['x'],
            Param::Arg::Value['y']]
        }
      }
    }

    merge_block(&teardownArgv)

    should('have no effect without limits provided') {
      sut << Param::Mod::RequireSomeFreeArgs[]
    }

    with(args: [ 'x=1', 'y=2', 'a', 'b', 'c' ]) {
      should('be ok when required min amount of free args is provided') {
        sut << Param::Mod::RequireSomeFreeArgs[atleast: 3]
      }

      should('be ok when required max amount of free args is provided/2') {
        sut << Param::Mod::RequireSomeFreeArgs[atmost: 3]
      }

      should('raise \'WrongNumberOfParameters\' when min amount of arguments is not provided') {
        assert_raise(Param::Ex::WrongNumberOfParameters) {
          sut << Param::Mod::RequireSomeFreeArgs[atleast: 4]
        }
      }

      should('raise \'WrongNumberOfParameters\' when more than max amount of arguments is not provided') {
        assert_raise(Param::Ex::WrongNumberOfParameters) {
          sut << Param::Mod::RequireSomeFreeArgs[atmost: 2]
        }
      }

      should('raise \'WrongNumberOfParameters\' when reassign decreases number of arguments below min value') {
        sut << Param::Arg::Value['z']

        sut << Param::Mod::RequireSomeFreeArgs[atleast: 3]

        assert_raise(Param::Ex::WrongNumberOfParameters) {
          sut << Param::Mod::ReassignFreeArgs['z']
        }
      }

      should('be ok when min args limits is lowered before reassign decreases number of arguments') {
        sut << Param::Arg::Value['z']

        sut << Param::Mod::RequireSomeFreeArgs[atleast: 3]
        sut << Param::Mod::RequireSomeFreeArgs[atleast: 2]

        sut << Param::Mod::ReassignFreeArgs['z']
      }
    }
  }
end


