# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'TestSupport'
require_relative 'UtUtility'

require_relative '../../lib/Param'

class TestParamArgValue < Test::Unit::TestCase
  context('Test Param::Arg::Value') {
    setup {
      (class << self; self; end).class_eval {
        define_method(:sut) {
          @sut ||= Param[Param::Arg::Value['--value']]
        }
      }
    }

    merge_block(&teardownArgv)

    should('return nil') {
      assert_equal(nil, sut['--value'])
    }

    should('return default value') {
      @sut = Param[Param::Arg::Value['--value', default: 72]]

      assert_equal('72', sut['--value'])
    }

    [ '-c=42', '--conf=42', '--config=42' ].each { |args_value|
      with(:args => args_value) {
        should('use alternative name') {
          @sut = Param[Param::Arg::Value['-c', altNames: ['--conf', '--config']]]

          assert_equal('42', sut['-c'])
          assert_equal('42', sut['--config'])
          assert_equal('42', sut['--conf'])
        }
      }
    }

    [ '--value=42', [ '--value', '42' ]].each { |args_value|
      with(:args => args_value) {
        should('return assigned value') {
          assert_equal('42', sut['--value'])
        }
      }
    }
  }
end
