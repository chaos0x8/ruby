# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'TestSupport'
require_relative 'UtUtility'

require_relative '../../lib/Param'

class TestParam < Test::Unit::TestCase
  context('TestParam') {
    setup {
      (class << self; self; end).class_eval {
        define_method(:sut) {
          @sut ||= Param[Param::Arg::Value['x']]
        }
      }
    }

    merge_block(&teardownArgv)

    [ ['x=14', 'y=42'], ['x', '14', 'y=42'], ['y', '42', 'x=14'] ].each { |args_value|
      with(args: args_value) {
        { 'via add' => proc { |s| s.add(Param::Arg::Value['y']) },
          'via stream operator' => proc { |s| s << Param::Arg::Value['y'] } }.each { |sufix, op|
          should("add new option #{sufix}") {
            op.call(sut)

            assert_equal('14', sut['x'])
            assert_equal('42', sut['y'])

            assert_equal([], sut.freeArgs)
          }
        }

        should('be ok when param is used after adding') {
          sut.add(Param::Arg::Value['y'], Param::Mod::AssumeFreeAsUnexpected[])
        }

        should('raise exception when mod added later') {
          assert_raise(Param::Ex::Unknown) {
            sut.add(Param::Mod::AssumeFreeAsUnexpected[])
          }
        }
      }
    }

    [ :description, :default ].each { |symbol|
      should("raise ScriptError when adding param with keep in #{symbol} for the first time") {
        assert_raise(ScriptError) {
          sut << Param::Arg::Value['y', symbol => Param::Arg::Keep]
        }
      }
    }

    context('override') {
      should('override param default value') {
        sut << Param::Arg::Value['x', default: 42]
        assert_equal('42', sut['x'])
      }

      should('not override param default value when set to \'Param::Arg::Keep\'') {
        @sut = Param[Param::Arg::Value['x', default: 12]]
        sut << Param::Arg::Value['x', default: Param::Arg::Keep]
        assert_equal('12', sut['x'])
      }

      with(args: 'x=13') {
        should('not override already assigned value') {
          sut << Param::Arg::Value['x', default: 42]
          assert_equal('13', sut['x'])
        }
      }

      with(args: 'z=14') {
        should('add altNames') {
          sut << Param::Arg::Value['x', altNames: ['z']]
          assert_equal('14', sut['x'])
        }

        should('removing altNames is not possible') {
          sut << Param::Arg::Value['x', default: 0, altNames: ['z']]
          sut << Param::Arg::Value['x', default: 0, altNames: []]
          assert_equal('14', sut['x'])
          assert_equal('14', sut['z'])
        }
      }

      should('override description') {
        @sut = Param[Param::Arg::Value['x', description: 'aaa']]
        sut << Param::Arg::Value['x', description: 'xyz']

        assert_not_match(/aaa/, sut.help)
        assert_match(/xyz/, sut.help)
      }

      should('not override description when set to \'Param::Arg::Keep\'') {
        @sut = Param[Param::Arg::Value['x', description: 'aaa']]
        sut << Param::Arg::Value['x', description: Param::Arg::Keep]

        assert_match(/aaa/, sut.help)
      }
    }
  }
end

