# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'TestSupport'
require_relative 'UtUtility'

require_relative '../../lib/Param'

class TestParamReassignFreeArgs < Test::Unit::TestCase
  context('TestParamReassignFreeArgs') {
    setup {
      (class << self; self; end).class_eval {
        define_method(:sut) {
          @sut ||= Param[
            Param::Arg::Value['x'],
            Param::Arg::Value['y'],
            Param::Arg::Value['z']
          ]
        }
      }
    }

    merge_block(&teardownArgv)

    with(args: [ '1', '2', '3', '4' ]) {
      should('reassign args when constructing') {
        @sut = Param[
          Param::Arg::Value['x'],
          Param::Arg::Value['y'],
          Param::Arg::Value['z'],
          Param::Mod::ReassignFreeArgs['x', 'z'],
          Param::Mod::ReassignFreeArgs['y', 'z']
        ]

        assert_equal('1', sut['x'])
        assert_equal('2', sut['z'])
        assert_equal('3', sut['y'])
        assert_equal(['4'], sut.freeArgs)
      }

      should('reassign args after constructing') {
        sut << Param::Mod::ReassignFreeArgs['x', 'z']
        sut << Param::Mod::ReassignFreeArgs['y']

        assert_equal('1', sut['x'])
        assert_equal('2', sut['z'])
        assert_equal('3', sut['y'])
        assert_equal(['4'], sut.freeArgs)
      }

      should('param can be reassigned many times afterconstructing, but warning is dusplayed') {
        $stdout.expects(:puts).at_least_once

        sut << Param::Mod::ReassignFreeArgs['y']
        sut << Param::Mod::ReassignFreeArgs['y']

        assert_equal('2', sut['y'])
        assert_equal(['3', '4'], sut.freeArgs)
      }
    }
  }
end

