# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'TestSupport'
require_relative 'UtUtility'

require_relative '../../lib/Param'

class TestParamArgSet < Test::Unit::TestCase
  context('Test Param::Arg::Set') {
    setup {
      (class << self; self; end).class_eval {
        define_method(:sut) {
          @sut ||= Param[Param::Arg::Set['--set']]
        }
      }
    }

    merge_block(&teardownArgv)

    should('return empty array') {
      assert_equal([], sut['--set'])
    }

    should('return default value') {
      @sut = Param[Param::Arg::Set['--set', default: [ 42, '72' ]]]

      assert_equal(['42', '72'], sut['--set'])
    }

    with(args: [ '--set=10', '-s=17', '--alt=42' ]) {
      should('use alternative name') {
        @sut = Param[Param::Arg::Set['--set', altNames: [ '--alt', '-s' ]]]

        assert_equal(['10', '17', '42'], sut['--set'])
        assert_equal(['10', '17', '42'], sut['--alt'])
        assert_equal(['10', '17', '42'], sut['-s'])
      }
    }

    [ [ '--set=val1', '--set=val2' ], [ '--set', 'val1', '--set', 'val2' ]].each { |args_value|
      with(:args => args_value) {
        should('return assigned values') {
          assert_equal(['val1', 'val2'], sut['--set'])
        }
      }
    }
  }
end
