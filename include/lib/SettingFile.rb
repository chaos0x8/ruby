#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016 - 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'settingfile/Storage'

class SettingFile
  class MissingRequired < RuntimeError

  end

  attr_reader :fileName

  def initialize(fileName)
    @fileName = fileName
    @details = Array.new
  end

  def register(key, **opts)
    fillDetails(key, **opts)

    register_read(key, **opts)
    register_write(key, **opts)

    if opts.has_key?(:default)
      send(self.class.key_write(key), opts[:default]) unless send(self.class.key_check(key))
    end
  end

  def frozen_clone
    clone = SettingFile.new(nil)
    @details.each { |name:, **opts|
      clone.fillDetails(name, frozen: true)

      val =
        if send(self.class.key_check(name))
          begin
            send(name).clone
          rescue TypeError
            send(name)
          end
        end
      clone.register_frozen(name, value: val)
    }
    clone
  end

  def details
    @details.collect { |info|
      detail =
        info.reject { |_, value|
          value.nil?
        }.collect { |key, value|
          case key
          when :name, :default
            "#{key}: #{value}"
          else
            if value == true
              "+#{key}"
            end
          end
        }.reject { |x| x.nil? }.join(', ')

      currentValue =
        begin
          send(info[:name])
        rescue MissingRequired
          nil
        end

      if not currentValue.nil?
        "#{detail}\n  value: #{send(info[:name])}"
      else
        require_relative 'Colorize'
        "#{detail}\n  value: #{'<nil>'.faint}"
      end
    }.join("\n")
  end

  def == other
    if other.class == SettingFile
      fields_values == other.fields_values
    else
      false
    end
  end

  def != other
    ! self.==(other)
  end

protected
  def fields_values
    @details.collect { |name:, **opts|
      field = Array.new
      field << send(self.class.key_check(name))
      field << send(name) if send(self.class.key_check(name))
      field
    }
  end

  def fillDetails(key, **opts)
    @details << {
      name: key,
      default: (opts.has_key?(:default) ? opts[:default] : nil),
      required: (opts.has_key?(:required) and opts[:required] ? true : false),
      collect: (opts.has_key?(:collect) and opts[:collect] ? true : false),
      store: (opts.has_key?(:store) and opts[:store] ? true : false),
      frozen: (opts.has_key?(:frozen) ? opts[:frozen] : false)
    }
  end

  def register_frozen(key, value:)
    (class << self; self; end).class_eval {
      define_method(key) {
        value
      }

      define_method(key_check(key)) {
        !! value
      }
    }
  end

private
  def self.key_check key
    :"#{key}?"
  end

  def self.key_write key
    :"#{key}="
  end

  def register_read(key, **opts)
    (class << self; self; end).class_eval {
      define_method(key) {
        v = Storage.read(@fileName, key)
        raise MissingRequired.new if v.nil? and opts[:required]
        (opts[:collect].respond_to?(:call) ? opts[:collect].call(v) : v)
      }

      define_method(key_check(key)) {
        !! Storage.read(@fileName, key)
      }
    }
  end

  def register_write(key, **opts)
    (class << self; self; end).class_eval {
      define_method(key_write(key)) { |val|
        raise MissingRequired.new if val.nil? and opts[:required]
        transformedVal = (opts[:store].respond_to?(:call) ? opts[:store].call(val) : val)
        Storage.write(@fileName, key => transformedVal)
        nil
      }
    }
  end
end
