#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016 - 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'thwait'
require 'etc'

class Parallel
  attr_reader :result

  def initialize(
    input:,
    nproc: (Etc.respond_to?(:nprocessors) ? Etc.nprocessors : Integer(`nproc`.chomp)),
    process_proc:,
    collect_proc: nil,
    join_proc: nil)

    @input = input
    @nproc = nproc
    @process_proc = process_proc
    @collect_proc = collect_proc
    @join_proc = join_proc

    @result = Array.new
    @threads = Array.new

    run_threading
  end

  def join
    @sync.join
    self
  end

private
  def run_threading
    @sync = Thread.new {
      [ @nproc, @input.size ].min.times {
        @threads << newThread
      }

      until @input.empty?
        thread = next_wait

        @result << collect_proc( thread[:output] )

        it = @threads.index(thread)
        @threads[it] = newThread
      end

      ThreadsWait.all_waits(*@threads) { |t|
        @result << collect_proc( t[:output] )
      }

      @result = join_proc( @result )
    }
  end

  def collect_proc output
    if @collect_proc
      @collect_proc.call( output )
    else
      output
    end
  end

  def join_proc output
    if @join_proc
      @join_proc.call( output )
    else
      output
    end
  end

  def next_wait
    thWait = ThreadsWait.new(*@threads)
    thWait.next_wait
  end

  def newThread
    t = Thread.new(@input.first) { |i|
      Thread.current[:output] = @process_proc.call( i )
    }

    @input.delete_at( 0 )

    t
  end
end
