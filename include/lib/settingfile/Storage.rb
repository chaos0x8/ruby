#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016 - 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class SettingFile
  module Storage
    class InvalidKeyError < RuntimeError
      def initialize msg
        super "InvalidKeyError: #{msg}"
      end
    end

    class Proxy
      def initialize fileName
        @fileName = fileName
        @tmp = nil
        @data = nil
      end

      def [] key
        Storage.symbolValid? key

        data.merge(@tmp || Hash.new)[key.to_sym]
      end

      def []= key, value
        Storage.symbolValid? key

        @tmp ||= Hash.new
        @tmp[key.to_sym] = (value.nil? ? nil : value.to_s)
      end

      def write
        if @tmp
          toSave = data.merge(@tmp).reject { |k, v| v.nil? }.to_a
          toSave.sort! { |a, b| a[0].to_s <=> b[0].to_s }

          File.open(@fileName, 'w') { |f|
            toSave.each { |k, v|
              f.write "#{k}=#{v}\n"
            }
          }
        end
      end

    private
      def data
        File.open(@fileName, 'r') { |f|
          @data = Hash.new

          f.read.each_line { |line|
            if m = line.match(/^(.*?)=(.*)$/)
              Storage.symbolValid? m[1]
              @data[m[1].to_sym] = m[2]
            end
          }
        } if @data.nil? and File.exist?(@fileName)

        @data ||= Hash.new
      end
    end

    def symbolValid? symbol
      eval ":#{symbol}"
    rescue SyntaxError => e
      raise InvalidKeyError.new(e.to_s)
    end

    def self.read fileName, *arguments
      storage = Storage::Proxy.new(fileName)
      result = Array.new
      arguments.each { |key|
        result << storage[key]
      }

      yield(*result) if block_given?

      case result.size
      when 0
        nil
      when 1
        result.first
      else
        result
      end
    end

    def self.write fileName, arguments = Hash.new
      storage = Storage::Proxy.new(fileName)
      arguments.each { |key, value|
        storage[key] = value
      }
      storage.write
      nil
    end

    module_function :symbolValid?
  end
end
