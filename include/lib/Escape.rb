#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

module Escape
  def self.join arr, separator = ';'
    return nil if arr.empty?

    arr.collect { |x|
      x.to_s.gsub('\\', '\\\\\\').gsub(separator, "\\#{separator}")
    }.join(separator).encode('utf-8')
  end

  def self.split txt, separator = ';'
    return [] if txt.nil?

    Enumerator.new { |e|
      word = ''
      esc = false

      txt.each_char { |ch|
        if esc
          word << ch
          esc = false
        else
          if ch == '\\'
            esc = true
          elsif ch == separator
            e << word
            word = ''
          else
            word << ch
          end
        end
      }

      e << word
    }.to_a
  end
end
