#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'Encoding'

require 'net/imap'

module Imap
  module Operation
    module Detail
      class Container
        attr_reader :value

        def initialize
          @value = []
        end

        def << toDel
          @value << toDel
          @value = @value.flatten.uniq.reject { |x| x.nil? }
          self
        end

        def include? val
          @value.include? val
        end

        def size
          @value.size
        end
      end

      module WithContainer
        include Imap::Encoding

        def withContainer imap, box, userBlock
          imap.select(e_utf7 box)
          container = Detail::Container.new
          userBlock.call container
          container = container.value
          yield container if container.size > 0
        ensure
          imap.close
        end
      end

      class Map
        attr_reader :value
        def initialize
          @value = {}
        end

        def << toMove
          unless toMove.nil?
            toMove.each { |obj, tgt|
              @value[tgt] ||= []
              @value[tgt] << obj
              @value[tgt] = @value[tgt].flatten.uniq.reject { |x| x.nil? }
            }
            @value = @value.reject { |tgt, obj| obj.empty? }
          end
        end

        def size
          @value.size
        end
      end

      module WithMap
        include Imap::Encoding

        def withMap imap, box, userBlock
          imap.select(e_utf7 box)
          map = Detail::Map.new
          userBlock.call map
          map = map.value
          yield map if map.size > 0
        ensure
          imap.close
        end
      end
    end

    class Move
      require_relative 'Utility'

      extend Detail::WithMap
      extend Utility

      def self.do imap, box, &block
        repeat = false

        withMap(imap, box, block) { |toMove|
          tgt, obj = toMove.first

          mkdir imap, tgt

          imap.copy(obj, tgt)
          imap.store(obj, '+FLAGS', [:Deleted])
          repeat = true if toMove.size > 1
        }

        self.do imap, box, &block if repeat
      end
    end

    class Delete
      extend Detail::WithContainer

      def self.do imap, box, &block
        withContainer(imap, box, block) { |toDel|
          imap.store(toDel, '+FLAGS', [:Deleted])
        }
      end
    end

    class See
      extend Detail::WithContainer

      def self.do imap, box, &block
        withContainer(imap, box, block) { |toSee|
          imap.store(toSee, '+FLAGS', [:Seen])
        }
      end
    end

    class Examine
      def self.do imap, box
        imap.examine(box)
        yield
      ensure
        imap.close
      end
    end
  end
end
