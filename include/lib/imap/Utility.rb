#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'net/imap'

module Imap
  module Utility
    def unmanagedImap(uri, port: 993, ssl: true, user:, password:)
      srv = Net::IMAP.new(uri, port: port, ssl: ssl)
      begin
        srv.login(user, password)
      rescue Exception
        srv.disconnect
        raise
      end
      srv
    end

    def manageImap(uri, port: 993, ssl: true, user:, password:)
      srv = Net::IMAP.new(uri, port: port, ssl: ssl)
      begin
        srv.login(user, password)
        begin
          yield srv
        ensure
          srv.logout
        end
      ensure
        srv.disconnect
      end
    end

    def decBase64 text
      warn '[DEPRECATED] `decBase64\' is deprecated. Use `Imap::Encofing.d_utf8\'.'
      require_relative 'Encoding'
      Imap::Encoding.d_utf8 text
    end

    def names from
      if from.respond_to? :collect
        from.collect { |item|
          Imap::Encoding.d_utf8 item.name
        }.uniq
      else
        []
      end
    end

    def mailboxes from
      if from.respond_to? :collect
        from.collect { |item|
          item.mailbox
        }.uniq
      else
        []
      end
    end

    def addresses from
      if from.respond_to? :collect
        from.collect { |item|
          "#{item.mailbox}@#{item.host}"
        }
      else
        []
      end
    end

    def hosts from
      if from.respond_to? :collect
        from.collect { |item|
          item.host
        }.uniq
      else
        []
      end
    end

    def mkdir imap, dir
      if ! imap.list("#{File.dirname(dir)}/", File.basename(dir))
        imap.create(dir)
      end
    end

    def rmdir imap, dir
      if imap.list("#{File.dirname(dir)}/", File.basename(dir))
        imap.delete(dir)
      end
    end

    module_function :unmanagedImap, :manageImap, :addresses, :hosts, :mkdir, :rmdir
  end
end
