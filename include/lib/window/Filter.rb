# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

begin
    require "gtk2"
rescue LoadError
    raise RuntimeError.new("This functionality requires 'ruby-gtk2'")
end

module Win

class FilterWindow < Gtk::Window
    attr_reader :result

    def initialize input
        super()

        @input = input
        @result = nil

        set_title "Filter out"
        set_window_position Gtk::Window::POS_CENTER

        initWidgets
        initLayout
        initSignals

        show_all
    end

    def initWidgets
        @buttons = [ Gtk::Button.new("Ok"),
                     Gtk::Button.new("Cancel") ]

        @checkBoxes = []
        @input.each do |v|
            box = Gtk::CheckButton.new(v)
            box.set_active true
            @checkBoxes.push box
        end
    end
    private :initWidgets

    def initLayout
        checkBox = Gtk::VBox.new
        @checkBoxes.each { |w| checkBox.pack_start w }

        checkScroll = Gtk::ScrolledWindow.new(nil, nil)
        checkScroll.set_size_request 400, 200
        checkScroll.set_policy( Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS )
        checkScroll.add_with_viewport checkBox

        buttonsBox = Gtk::HBox.new
        @buttons.each { |w| buttonsBox.pack_start w }

        windowBox = Gtk::VBox.new
        windowBox.pack_start checkScroll
        windowBox.pack_start buttonsBox, false

        add windowBox
    end
    private :initLayout

    def initSignals
        @buttons[0].signal_connect "clicked" do
            @result = collectResult
            destroy
        end

        @buttons[1].signal_connect "clicked" do
            @result = nil
            destroy
        end

        signal_connect "destroy" do
            Gtk.main_quit
        end
    end
    private :initSignals

    def collectResult
        result = []

        @checkBoxes.each do |cb|
            result.push(cb.label) if cb.active?
        end

        result
    end
    private :collectResult
end

def self.filterOut values
    Gtk.init

    filter = FilterWindow.new(values)

    Gtk.main

    filter.result or Array.new
end

end
