# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

begin
    require "gtk2"
rescue LoadError
    raise RuntimeError.new("This functionality requires 'ruby-gtk2'")
end

module Win

class Entry < Gtk::Window
    attr_reader :result

    def initialize prompt, description
        super()

        @result = nil

        set_title "Entry dialog"
        set_window_position Gtk::Window::POS_CENTER

        initWidgets prompt, description
        initLayout
        initSignals

        show_all
    end

    def initWidgets prompt, description
        @buttons = [ Gtk::Button.new("Ok"),
                     Gtk::Button.new("Cancel") ]
        @prompt = Gtk::Label.new(prompt)
        @description = Gtk::Label.new(description)
        @answer = Gtk::TextView.new
        @answer.set_size_request 200, 60
    end
    private :initWidgets

    def initLayout
        descriptionScroll = Gtk::ScrolledWindow.new(nil, nil)
        descriptionScroll.set_policy( Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC )
        descriptionScroll.set_size_request 700, 200
        descriptionScroll.add_with_viewport @description

        panned = Gtk::VPaned.new
        panned.pack1 descriptionScroll, true, false
        panned.pack2 @answer, true, false

        buttonsBox = Gtk::HBox.new
        @buttons.each { |w| buttonsBox.pack_start w, false }

        windowBox = Gtk::VBox.new
        windowBox.pack_start @prompt, false
        windowBox.pack_start panned
        windowBox.pack_start buttonsBox, false

        add windowBox
    end
    private :initLayout

    def initSignals
        @buttons[0].signal_connect "clicked" do
            @result = @answer.buffer.text
            destroy
        end

        @buttons[1].signal_connect "clicked" do
            @result = nil
            destroy
        end

        signal_connect "destroy" do
            Gtk.main_quit
        end
    end
    private :initSignals
end

def self.getAnswer prompt, description
    Gtk.init

    filter = Entry.new(prompt, description)

    Gtk.main

    filter.result or ""
end

end
