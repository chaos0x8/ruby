# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'gtk2'

module Question

class MainWindow < Gtk::Window
    attr_reader :result

    def initialize question
        super

        @result = nil

        set_title "Question box"
        set_size_request 400, 100
        set_window_position Gtk::Window::POS_CENTER

        initUI question

        show_all
    end

    def initUI question
        box1 = Gtk::VBox.new
        add box1

        label = Gtk::Label.new question
        box1.add label

        box2 = Gtk::HBox.new
        box1.pack_start box2, false

        ok = Gtk::Button.new "Tak"
        box2.add ok

        cancel = Gtk::Button.new "Nie"
        box2.add cancel

        ok.signal_connect "clicked" do
            @result = true
            destroy
        end

        cancel.signal_connect "clicked" do
            @result = false
            destroy
        end

        signal_connect "destroy" do
            Gtk.main_quit
        end
    end
end

def self.ask question
    Gtk.init
        win = MainWindow.new question
    Gtk.main

    win.result
end

end
