#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class Param
  class Arg
    class Detail
      class Base
        class AdvField
          attr_reader :value

          def initialize v
            if v == Param::Arg::Keep
              @keep = true
              self.value = nil
            else
              @keep = false
              self.value = v
            end
          end

          def to_s
            value.to_s
          end

          def value?
            ! @value.nil?
          end

          def keep?
            !! @keep
          end
        end

        class Default < Arg::Detail::Base::AdvField; end

        class Description < Arg::Detail::Base::AdvField
          def value= v
            @value = v.to_s
          end
        end

        attr_reader :name

        def initialize(name, default: nil, description: nil, altNames: [])
          @name = name
          @default = self.class::Default.new(default)
          @altNames = altNames
          @description = self.class::Description.new(description)
          @value = nil
        end

        def value
          (@value.nil? ? @default.value : @value)
        end

        def overrideBy other
          @altNames = (@altNames + other.altNames).uniq
          @description = other.description unless other.description.keep?
          @default = other.default unless other.default.keep?
        end

        def keep?
          [ :description, :default ].any? { |sym| send(sym).keep? }
        end

        def == other
          if other.kind_of? Param::Arg::Detail::Base
            [ @name ] == [ other.name ]
          else
            [ @name, @altNames ].flatten.include?(other)
          end
        end

        def to_s
          text = @name
          text += " - #{@description}" if @description.value?
          text += "\n\tdefault: #{@default}" if @default.value?
          text += "\n\taltNames: #{@altNames.join(', ')}" if @altNames.size > 0
          text
        end

      protected
        attr_reader :altNames, :description, :default

      private
        def reasignWarning
          require 'Colorize'
          $stdout.puts "Warning: param '#{@name}' is reassigned".red
        end
      end
    end

    class Option < Param::Arg::Detail::Base
      class Default < Param::Arg::Detail::Base::Default
        def value= v
          @value = !! v
        end
      end

      def self.[] *args
        self.new(*args)
      end

      def insert arg
        reasignWarning unless @value.nil?

        @value = (arg.nil? or arg == 'true') ? true : false
      end
    end

    class Value < Param::Arg::Detail::Base
      class Default < Param::Arg::Detail::Base::Default
        def value= v
          @value = (v.nil?) ? nil : v.to_s
        end
      end

      def self.[] *args
        self.new(*args)
      end

      def insert arg
        reasignWarning unless @value.nil?

        @value = arg
      end
    end

    class Set < Param::Arg::Detail::Base
      class Default < Param::Arg::Detail::Base::Default
        def value= v
          @value = (v.nil?) ? Array.new : v.collect { |x| x.to_s }
        end
      end

      def self.[] *args
        self.new(*args)
      end

      def insert arg
        @value ||= Array.new
        @value << arg
      end
    end

    class Keep; end
  end
end
