#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class Param
  class Mod
    def initialize(*args, **opts)

    end

    def self.[](*args, **opts)
      self.new(*args, **opts)
    end

    def unique?
      false
    end

    class AssumeFreeAsUnexpected < Param::Mod; end

    class RequireSomeFreeArgs < Param::Mod
      def initialize(atleast: nil, atmost: nil)
        @atleast = atleast
        @atmost = atmost
      end

      def unique?
        true
      end

      def overrideBy other
        @atleast = other.atleast
        @atmost = other.atmost
      end

      def check(provided)
        unless (@atleast.nil? or provided >= @atleast) and
               (@atmost.nil? or provided <= @atmost)
          raise Param::Ex::WrongNumberOfParameters.new(
            atleast: @atleast, atmost: @atmost, provided: provided)
        end
      end

    protected
      attr_reader :atleast, :atmost
    end

    class ReassignFreeArgs < Param::Mod
      attr_accessor :vals

      def unique?
        true
      end

      def initialize(*vals, **opts)
        @vals = vals
      end

      def overrideBy other
        @vals = (@vals + other.vals).uniq
      end
    end

    class MandatoryArgs < Param::Mod
      attr_accessor :vals

      def initialize(*vals, **opts)
        @vals = vals
      end

      def missingParams registeredParams
        missing = @vals.select { |val|
          not registeredParams.index { |rp| rp.name == val and not rp.value.nil? }
        }

        (missing.size > 0 ? missing : nil)
      end

      class AnyOf < MandatoryArgs
        def initialize(*vals, **opts)
          tmp = vals.collect { |x|
            if x.kind_of? MandatoryArgs
              x
            elsif x.kind_of? Array
              MandatoryArgs[*x]
            else
              MandatoryArgs[x]
            end
          }

          super(*tmp)
        end

        def missingParams registeredParams
          missing = @vals.collect { |x| x.missingParams(registeredParams) }
          (missing.none? { |x| x.nil? } ? missing : nil )
        end
      end
    end
  end
end
