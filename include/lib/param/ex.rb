#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class Param
  class Ex
    class Error < RuntimeError; end

    class Unknown < Param::Ex::Error
      def initialize params
        super "Unknown params: #{params}"
      end
    end

    class MissingMandatory < Param::Ex::Error
      def initialize *paramNames
        errorList = paramNames.collect { |p|
          if p.kind_of? Array
            "(#{p.join(', ')})"
          else
            p
          end
        }

        super("Missing mandatory parameter '#{errorList.join(' or ')}'")
      end
    end

    class WrongNumberOfParameters < Param::Ex::Error
      def initialize(atleast:, atmost:, provided:)
        expected = Array.new
        expected << "atleast: #{atleast}" if atleast
        expected << "atmost: #{atmost}" if atmost
        super("Not enough parameters! expected: #{expected.join(' and ')} but provided #{provided}")
      end
    end
  end
end

