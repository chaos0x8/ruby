#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'Colorize'
require_relative 'param/arg'
require_relative 'param/ex'
require_relative 'param/mod'

class Param
  attr_reader :freeArgs

  def initialize *optionsAndMods
    @registeredParams, @mods = Array.new, Array.new
    @freeArgs = ARGV.clone
    add(*optionsAndMods)
  end

  def self.[] *args
    self.new(*args)
  end

  def [] key
    find(key).value
  end

  def add *optionsAndMods
    params, mods = splitArgs(optionsAndMods)
    addParams(params)
    addMods(mods)

    assignFreeArgsToParams

    reassignFreeArgs
    checkMandatoryParameters
    assumeThatFreeParamsAreUnexpected if mod(Param::Mod::AssumeFreeAsUnexpected)
    mod(Param::Mod::RequireSomeFreeArgs) { |m|
      m.check(freeArgs.size)
    }
  end

  def << optionOrMod
    self.add(optionOrMod)
    self
  end

  def help
    self.class._help(@registeredParams)
  end

  def self._help params
    params = params.reject { |x| x.kind_of? Param::Mod }
    params.join("\n")
  end

private
  def addParams params
    params.each { |param|
      if it = @registeredParams.index { |x| x == param }
        @registeredParams[it].overrideBy param
      else
        raise ScriptError.new('Unexpected Keep! There is no previous value') if param.keep?
        @registeredParams << param
      end
    }
  end

  def addMods mods
    mods.each { |newMod|
      if newMod.unique? and oldMod = mod(newMod.class)
        oldMod.overrideBy(newMod)
      else
        @mods << newMod
      end
    }
  end

  def find key
    if it = @registeredParams.index { |x| x == key }
      @registeredParams[it]
    else
      raise Param::Ex::Unknown.new(key)
    end
  end

  def assignFreeArgsToParams
    to_delete = Array.new

    @freeArgs.each_with_index { |arg, it|
      next if to_delete.include? it

      begin
        if m = arg.match(/^(.+?)=(.*)$/)
          find(m[1]).insert m[2]
        elsif find(arg).class == Param::Arg::Option
          find(arg).insert(nil)
        else
          find(arg).insert(@freeArgs[it+1])
          to_delete << it+1
        end
        to_delete << it
      rescue Param::Ex::Unknown; end
    }

    to_delete.sort.reverse.each { |it| @freeArgs.delete_at(it) }
  end

  def checkMandatoryParameters
    mod(Param::Mod::MandatoryArgs) { |mandatoryMod|
      if missing = mandatoryMod.missingParams(@registeredParams)
        raise Param::Ex::MissingMandatory.new(*missing)
      end
    }
  end

  def assumeThatFreeParamsAreUnexpected
    raise Param::Ex::Unknown.new("#{@freeArgs.join(", ")}") unless @freeArgs.empty?
  end

  def splitArgs optionsAndMods
    opts = optionsAndMods.reject { |x| x.kind_of? Param::Mod }
    mods = optionsAndMods.select { |x| x.kind_of? Param::Mod }

    [ opts, mods ]
  end

  def mod modClass
    if block_given?
      @mods.each { |m|
        yield(m) if m.kind_of? modClass
      }
    else
      if it = @mods.index { |x| x.kind_of? modClass }
        return @mods[it]
      end
    end
  end

  def reassignFreeArgs
    dropCount = 0
    @freeArgs.each { |arg|
      if reassign = mod(Param::Mod::ReassignFreeArgs) and reassign.vals.size > 0
        it = @registeredParams.index { |x| x.name == reassign.vals.first }
        if [Param::Arg::Option, Param::Arg::Value, Param::Arg::Set].include? @registeredParams[it].class
          find(reassign.vals.first).insert arg
          reassign.vals = reassign.vals.drop(1) unless @registeredParams[it].class == Param::Arg::Set
          dropCount += 1
        end
      end
    }
    @freeArgs = @freeArgs.drop(dropCount)
  end
end
