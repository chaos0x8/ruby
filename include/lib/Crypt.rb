#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2017, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

module Crypt
  module Detail
    def sha256 text
      require 'digest'
      sha = Digest::SHA256.new
      sha << text
      sha.digest
    end

    def md5 text
      require 'digest'
      md5 = Digest::MD5.new
      md5 << text
      md5.digest
    end

    module_function :sha256, :md5
  end

  def e_aes data, pass1, pass2 = ENV['USER']
    require 'openssl'

    cipher = OpenSSL::Cipher::AES256.new(:CBC)
    cipher.encrypt
    cipher.key = Detail.sha256(pass1)
    cipher.iv = Detail::md5(pass2)
    cipher.update(data) + cipher.final
  end

  def d_aes data, pass1, pass2 = ENV['USER']
    require 'openssl'

    cipher = OpenSSL::Cipher::AES256.new(:CBC)
    cipher.decrypt
    cipher.key = Detail.sha256(pass1)
    cipher.iv = Detail::md5(pass2)
    cipher.update(data) + cipher.final
  end

  def e_base64 data
    require 'base64'
    Base64.encode64(data).chomp
  end

  def d_base64 data
    require 'base64'
    Base64.decode64(data)
  end

  module_function :e_aes, :d_aes, :e_base64, :d_base64
end
