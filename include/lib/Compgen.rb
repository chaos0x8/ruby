#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

module Compgen
  module Detail
    def self.compgenBase(values, patern)
      values.select { |x| x.match(patern) }
    end
  end

  def commonStr(*values)
    unless values.empty?
      minSize = values.collect(&:size).min
      minSize.downto(1) { |s|
        truncated = values.collect { |x| x[0..s] }
        return truncated.first if truncated.uniq.size == 1
      }
    end
    ''
  end

  def compgen(values: [], patern: '')
    Detail::compgenBase(values, /^#{Regexp.quote(patern)}/)
  end

  def compgenEx(values: [], patern: '')
    Detail::compgenBase(values, /#{Regexp.quote(patern)}/)
  end
end

