Gem::Specification.new { |s|
  s.name        = 'include'
  s.version     = '0.6.7'
  s.date        = File.mtime(__FILE__).strftime('%Y-%m-%d')
  s.summary     = "#{s.name} library"
  s.description = "Base for other scripts"
  s.authors     = ["chaos0x8"]
  s.files       = Dir['lib/**/*.rb']
  s.add_development_dependency 'test-support', '~> 0.0.0'
}
