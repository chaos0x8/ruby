Gem::Specification.new { |s|
  s.name        = 'cpp-diagrams'
  s.version     = '0.0.1'
  s.date        = '2016-04-09'
  s.summary     = "#{s.name} library"
  s.description = "Generated dot diagramns for c++ from tags file"
  s.authors     = ["chaos0x8"]
  s.files       = Dir['lib/*.rb', 'bin/*.rb']
  s.executables = Dir['bin/*.rb'].collect { |x| File.basename(x) }
}
