#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'DotDrawUtilities'
require_relative 'DotDrawClass'

class DotDraw
    class Group < Utilities::Indentable
        attr_reader :clas

        def initialize setting, name
            super(setting, name)
            @clas = Classes.new(setting)
        end

        def indent= value
            @indent = value
            @clas.indent = value + 2
        end

        def to_s
            result = Array.new

            result.push "subgraph cluster#{@name.gsub('::', '_')} {"
            result.push "  label = \"#{@name}\""
            result.push ""
            result.push @clas
            result.push "}"

            result.collect{|i| "#{indent}#{i}"}.join("\n")
        end
    end

    class Groups < Utilities::HashContainer
        def [] name
            access(Group, name)
        end

        def to_s
            set_all :indent, @indent
            @container.join("\n\n")
        end
    end
end
