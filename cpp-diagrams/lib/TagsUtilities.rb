#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'DotDrawAccess'

module TagsUtilities
    def tagsAccessToDotAccess access
        map = { 'public' => DotDraw::Access::PUBLIC,
                'private' => DotDraw::Access::PRIVATE,
                'protected' => DotDraw::Access::PROTECTED }
        map[access]
    end

    def invalidTag? tagName
        [ 'noexcept', 'override' ].include?(tagName)
    end

    def typeFromTagPatern tag
        type = tag.patern.match(/(\w[\w\s<>:]*)\s+#{tag.name}/)[1].squeeze(' ').strip rescue nil

        if type
            return type.gsub(/\b(static|constexpr)\b/, '').strip
        end

        nil
    end

    def typedefFromTagPatern tag
        type = tag.patern.match(/typedef\s+(.*)\s+#{tag.name}/)[1].squeeze(' ').strip rescue nil
    end

    def usingFromTagPatern tag
        type = tag.patern.match(/using\s+#{tag.name}\s*=\s*(.*)\s*;/)[1].squeeze(' ').strip rescue nil
    end

    alias_method :returnTypeFromTagPatern, :typeFromTagPatern
end
