#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'TagsType'

class TagsEntry
    include TagsType

    attr_reader :name, :fileName, :patern, :extras

    @@element = /(.*?)[\t\n]/

    def initialize line
        m = greedyMatch(line)

        @name = m[1]
        @fileName = m[2]
        @patern = m[3].match(/(.*);"/)[1]
        @extras = parseExtras(m[4..m.size-1])
    end

    def to_s
        "#{@name} #{File.basename(@fileName)} #{@extras}"
    end

  private
    def greedyMatch line
        result = nil
        matchResult = nil

        times = 3
        begin
            result = matchResult
            matchResult = line.match(/#{@@element.to_s*times}/)
            times += 1
        end while matchResult

        result
    end

    def parseExtras extras
        result = Hash.new

        extras.each { |extra|
            if m = extra.match(/^\w$/)
                result[:type] = translateType(m[0])
            elsif m = extra.match(/^(\w+):(.+)$/)
                result[m[1].to_sym] = m[2]
            end
        }

        result
    end
end
