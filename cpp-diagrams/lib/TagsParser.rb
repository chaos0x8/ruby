#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'TagsEntry'
require_relative 'TagsUtilities'

class TagsParser
    include TagsUtilities

    def initialize fileName
        @content = File.open(fileName, 'r') { |f|
            f.each_line.reject { |l| l.match(/^!_/) rescue true }
        }
    end

    def toDraw dotDraw
        each { |tag|
            next if invalidTag?(tag.name)

            ns = ''
            ns = "#{tag.extras[:namespace]}::" if tag.extras[:namespace]

            case tag.extras[:type]
            when TagsType::CLASS, TagsType::STRUCT
                dotDraw.clas[ns + tag.name]
                if tag.extras[:inherits]
                    tag.extras[:inherits].split(',').each { |baseClass|
                        dotDraw.inheritance[ns + tag.name].target = ns + baseClass
                    }
                end
            when TagsType::MEMBER
                if tag.extras[:class]
                    dotDraw.clas[tag.extras[:class]].member[tag.name]
                    dotDraw.clas[tag.extras[:class]].member[tag.name].access = tagsAccessToDotAccess(tag.extras[:access]) if tag.extras[:access]
                    dotDraw.clas[tag.extras[:class]].member[tag.name].type = typeFromTagPatern(tag) if typeFromTagPatern(tag)
                end
            when TagsType::FUNCTION
                if tag.extras[:class]
                    dotDraw.clas[tag.extras[:class]].method[tag.name].rType = returnTypeFromTagPatern(tag) if returnTypeFromTagPatern(tag)
                    dotDraw.clas[tag.extras[:class]].method[tag.name].access = tagsAccessToDotAccess(tag.extras[:access]) if tag.extras[:access]
                    dotDraw.clas[tag.extras[:class]].method[tag.name].signature = tag.extras[:signature] if tag.extras[:signature]
                end
            when TagsType::TYPE
                dotDraw.typedef[ns + tag.name].type = typedefFromTagPatern(tag)
            when TagsType::USING
                dotDraw.typedef[ns + tag.name].type = usingFromTagPatern(tag)
            end
        }

        dotDraw.autoResolveTypedefs
        dotDraw.autoCompleteMemberRelations
    end

    def each
        @content.each { |line|
            yield TagsEntry.new(line)
        }
    end
end
