#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'DotDrawUtilities'

class DotDraw
    class Class < Utilities::Element
        class Method < Utilities::Element
            include Utilities

            attr_accessor :rType, :access, :signature

            def initialize setting, name
                super(setting, name)
                @rType = 'void'
                @access = Access::PUBLIC
                @signature = '()'
            end

            def to_s
                name = escape(@name)
                signature = escape(@signature, @setting.methodSignatureLimit)
                rType = escape((@rType ? " : #{@rType}" : ''))

                "#{@access} #{name}#{signature}#{rType}"
            end
        end

        class Methods < Utilities::HashContainer
            def [] name
                access(Method, name)
            end

            def to_s
                @container.join('\l') + '\l'
            end
        end
    end
end
