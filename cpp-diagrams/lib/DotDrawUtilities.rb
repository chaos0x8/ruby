#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class DotDraw
    module Utilities
        class Indentable
            attr_writer :indent

            def initialize setting
                @setting = setting
                @indent = 0
            end

            def indent
                ' ' * @indent
            end
        end

        class Element < Indentable
            attr_reader :name

            def initialize setting, name
                super(setting)
                @name = name
            end
        end

        class HashContainer < Indentable
            def initialize setting
                super(setting)
                @container = Array.new
            end

            def indent= value
                @indent = value
                set_all :indent, value
            end

            def access classType, name
                it = @container.index { |m| m.name == name }
                if it.nil?
                    @container.push(classType.new(@setting, name))
                    return @container.last
                end
                @container[it]
            end

            def set_all symbol, value
                assignSymbol = "#{symbol}=".to_sym

                @container.each { |item|
                    item.send(assignSymbol, value)
                }
            end

            def each
                @container.each { |item| yield item }
            end

            def include? *elements
                it = @container.index { |item|
                    elements.include?(item.name)
                }
                !! it
            end

            def findMatch name
                if it = @container.index { |item| item.name.match(/\b#{name}\b/) }
                    return @container[it].name
                end

                nil
            end

            def keep *toKeep
                @container = @container.select { |item| toKeep.include?(item.name) }
            end

            def size
                @container.size
            end
        end

        class ArrowHashContainer < HashContainer
            def initialize setting, arrowHead, arrowTail, direction
                super(setting)
                @arrowHead = arrowHead
                @arrowTail = arrowTail
                @direction = direction
            end

            def arrow
                result = Array.new

                result.push ""
                result.push "edge ["
                result.push "  arrowhead = \"#{@arrowHead}\""
                result.push "  arrowtail = \"#{@arrowTail}\""
                result.push "  dir = \"#{@direction}\""
                result.push "]"
                result.push ""

                result.collect{|i| "#{indent}#{i}"}.join("\n")
            end
        end

        def escape text, limit = nil
            result = text.gsub(/([<>=\+\-&])/, '\\\\\1')
            result = result[0..limit] + '...' if limit and result.size > limit
            result
        end

        def extractType type
            if type
                if m = type.match(/(shared_ptr|unique_ptr)\s*<\s*(const){0,1}\s*([\w:]+)\s*>/)
                    return m[3]
                elsif m = type.match(/^(const){0,1}\s*(\w+)$/)
                    return m[2]
                end
            end

            nil
        end
    end
end
