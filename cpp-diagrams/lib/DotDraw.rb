# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'DotDrawClass'
require_relative 'DotDrawGroup'
require_relative 'DotDrawRelation'
require_relative 'DotDrawTypedef'
require_relative 'DotDrawSetting'
require_relative 'DotDrawUtilities'

require 'set'

class DotDraw
    include DotDraw::Utilities

    attr_reader :setting
    attr_reader :clas, :group, :inheritance, :agregation, :typedef

    def initialize
        @setting = Setting.new

        @clas = Classes.new(@setting)
        @clas.indent = 2

        @group = Groups.new(@setting)
        @group.indent = 2

        @inheritance = Inheritances.new(@setting)
        @inheritance.indent = 2

        @agregation = Agregations.new(@setting)
        @agregation.indent = 2

        @typedef = Typedefs.new(@setting)
    end

    def autoResolveTypedefs
        @typedef.each { |typedef|
            clas = @clas[typedef.name]

            type = extractType(typedef.type) || typedef.type

            if resolvedType = resolveTargetClass(clas,type)
                @inheritance[clas.name].target = resolvedType
            end
        }
    end

    def autoCompleteMemberRelations
        @clas.each { |clas|
            clas.member.each { |member|
                if type = extractType(member.type)
                    if resolvedType = resolveTargetClass(clas, type)
                        @agregation[clas.name].target = resolvedType
                    end
                end
            }
        }
    end

    def keepRelatedTo *classes
        toKeep = Set.new(classes)

        sizeBefore = nil

        begin
            sizeBefore = toKeep.size

            toKeep.clone.each { |clas|
                toKeep.add(clas)

                toKeep += @inheritance[clas].targets if @inheritance.include?(clas)
                toKeep += @agregation[clas].targets if @agregation.include?(clas)

                @inheritance.each { |inh|
                    toKeep.add(inh.name) if inh.targets.include?(clas)
                }
            }
        end until sizeBefore == toKeep.size

        @clas.keep(*toKeep)
        @inheritance.keep(*toKeep)
        @agregation.keep(*toKeep)
    end

    def to_s
        result = Array.new

        result.push 'digraph G {'
        result.push '  fontname = "Bitstream Vera Sans"'
        result.push '  fontsize = 8'
        result.push ''
        result.push '  node ['
        result.push '    fontname = "Bitstream Vera Sans"'
        result.push '    fontsize = 8'
        result.push '    shape = "record"'
        result.push '  ]'
        result.push ''
        result.push '  edge ['
        result.push '    fontname = "Bitstream Vera Sans"'
        result.push '    fontsize = 8'
        result.push '  ]'
        if @clas.size > 0 # only to avoid too many empty lines
            result.push ''
            result.push @clas
        end
        if @group.size > 0 # only to avoid too many empty lines
            result.push ''
            result.push @group
        end
        [@inheritance, @agregation].each { |relations|
            result.push(relations) if relations.size > 0
        }
        result.push '}'

        result.join("\n").split("\n").collect{|l| l.gsub(/^\s+$/, '') }.join("\n")
    end

private
    def resolveTargetClass sourceClass, type
        types = Array.new

        if m = type.match(/^::(.*)/)
            return m[1] if @clas.include?(m[1])
            return nil
        end

        namespaceSkip = 0
        while ns = sourceClass.namespace(namespaceSkip)
            namespaceSkip += 1
            types.push(ns + type)
        end

        types.push(type)

        if @clas.include?(*types)
            types.each { |t|
                if @clas.include?(t)
                    return t
                end
            }
        elsif name = @clas.findMatch(type)
            return name
        end

        nil
    end
end
