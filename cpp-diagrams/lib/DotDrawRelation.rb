#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'DotDrawUtilities'

require 'set'

class DotDraw
    class Relation < Utilities::Element
        def initialize setting, name
            super(setting, name)
            @targets = Set.new
        end

        def target= value
            @targets.add(value)
        end

        def targets *items
            @targets = Set.new(*items)
        end

        def targets
            @targets.to_a
        end

        def to_s
            result = Array.new
            @targets.each { |t|
                result.push("#{indent}#{@name.gsub('::', '_')} -> #{t.gsub('::', '_')}")
            }
            result.join("\n")
        end
    end

    class ReversedRelation < Relation
        def to_s
            result = Array.new
            @targets.each { |t|
                result.push("#{indent}#{t.gsub('::', '_')} -> #{@name.gsub('::', '_')}")
            }
            result.join("\n")
        end
    end

    class Relations < Utilities::ArrowHashContainer
        def [] name
            access(Relation, name)
        end

        def to_s
            set_all :indent, @indent
            arrow + "\n" + @container.join("\n")
        end
    end

    class Inheritances < Relations
        def initialize setting
            super(setting, 'none', 'empty', 'both')
        end

        def [] name
            access(ReversedRelation, name)
        end
    end

    class Agregations < Relations
        def initialize setting
            super(setting, 'diamond', 'none', 'forward')
        end
    end
end
