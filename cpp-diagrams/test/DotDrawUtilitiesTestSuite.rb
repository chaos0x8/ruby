#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/DotDrawUtilities'

class TestElement < DotDraw::Utilities::Element
    attr_accessor :content
end

class TestHashContainer < DotDraw::Utilities::HashContainer
    def [] name
        access(TestElement, name)
    end
end

class DotDrawUtilitiesTestSuite < Test::Unit::TestCase
    include DotDraw::Utilities

    should('escape characters') {
        assert_equal('\<\>\=\+\-', escape('<>=+-'))
    }

    should('not escape characters') {
        assert_equal('hello world', escape('hello world'))
    }

    should('return nil when type is nil') {
        assert_equal(nil, extractType(nil))
    }

    should('extract type from shared_ptr') {
        @values = [ 'std::shared_ptr<Foo>',
                    'shared_ptr<Foo>',
                    'const std::shared_ptr<Foo>&',
                    'std::shared_ptr<const Foo>' ]

        @values.each { |v|
            assert_equal('Foo', extractType(v), "test value: #{v}")
        }
    }

    should('extract type from unique_ptr') {
        @values = [ 'std::unique_ptr<Foo>',
                    'unique_ptr<Foo>',
                    'const std::unique_ptr<Foo>&',
                    'std::unique_ptr<const Foo>' ]

        @values.each { |v|
            assert_equal('Foo', extractType(v), "test value: #{v}")
        }
    }

    should('extract type from value') {
        @values = [ 'const Foo',
                    'Foo' ]

        @values.each { |v|
            assert_equal('Foo', extractType(v), "test value: #{v}")
        }
    }

    should('return nil when extraction fails') {
        @values = [ 'Foo&',
                    'Foo*',
                    'const Foo&',
                    'const Foo*',
                    'const Foo* const' ]

        @values.each { |v|
            assert_equal(nil, extractType(v), "test value: #{v}")
        }
    }

    context('HashContainer') {
        setup {
            @setting = Object.new
            @sut = TestHashContainer.new(@setting)
        }

        should('create element with desired type') {
            assert_equal(true, @sut['key'].kind_of?(TestElement))
            assert_equal(1, @sut.size)
        }

        should('read previously writed value') {
            @sut['key'].content = 'val'

            assert_equal('val', @sut['key'].content)
        }

        should('return indent') {
            @sut.indent = 3

            assert_equal('   ', @sut.indent)
        }

        context('with 2 elements') {
            setup {
                @sut['elem_1']
                @sut['elem_2']
            }

            should('set indent in elements') {
                @sut.indent = 2

                assert_equal('  ', @sut['elem_1'].indent)
            }

            should('set value in all elements') {
                @sut.set_all(:content, 'same_val')

                assert_equal('same_val', @sut['elem_1'].content)
                assert_equal('same_val', @sut['elem_2'].content)
            }

            should('keep all elements') {
                @sut.keep('elem_1', 'elem_2')

                assert_equal(2, @sut.size)
            }

            should('keep one element') {
                @sut.keep('elem_1')

                assert_equal(1, @sut.size)
                assert_equal(true, @sut.include?('elem_1'))
            }

            context('with element with namespace') {
                setup {
                    @sut['Ns::elem_3']
                }

                should('find matching element') {
                    assert_equal('Ns::elem_3', @sut.findMatch('elem_3'))
                }

                should('not find matching element') {
                    assert_equal(nil, @sut.findMatch('elem'))
                }
            }
        }
    }
end
