#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/TagsUtilities'
require_relative '../lib/TagsEntry'

class TagsUtilitiesTestSuite < Test::Unit::TestCase
    include TagsUtilities

    [ :returnTypeFromTagPatern, :typeFromTagPatern ].each { |method|
        context("TagsUtilities.#{method}") {
            should('extract type from tag patern') {
                tag = TagsEntry.new("MemberName\tFileName\t/^    complicated type MemberName$/;\"\n")

                assert_equal('complicated type', self.send(method, tag))
            }

            should('return nil when type extraction fails') {
                tag = TagsEntry.new("MemberName\tFileName\t/^  some type  InvalidTag$/;\"\n")

                assert_equal(nil, self.send(method, tag))
            }

            should('extract return type from tag patern') {
                tag = TagsEntry.new("MethodName\tFileName\t/^  complicated type  MethodName()$/;\"\n")

                assert_equal('complicated type', self.send(method, tag))
            }

            should('remove prefix from extracted return type') {
                tag = TagsEntry.new("MethodName\tFileName\t/^  static  constexpr complicated type  MethodName()$/;\"\n")

                assert_equal('complicated type', self.send(method, tag))
            }

            should('return nil when return type extraction fails') {
                tag = TagsEntry.new("MethodName\tFileName\t/^  complicated type  InvalidTag()$/;\"\n")

                assert_equal(nil, self.send(method, tag))
            }
        }
    }

    context('TagsUtilities.typedefFromTagPatern') {
        should('extract type from typedef patern') {
            tag = TagsEntry.new("AliasName\tFileName\t/^typedef TypeName AliasName;$/;\"\n")

            assert_equal('TypeName', typedefFromTagPatern(tag))
        }

        should('return nil when typedef extraction fails') {
            tag = TagsEntry.new("AliasName\tFileName\t/^typedef TypeName WrongAlias;$/;\"\n")

            assert_equal(nil, typedefFromTagPatern(tag))
        }
    }

    context('TagsUtilities.usingFromTagPatern') {
        should('extract type from typedef patern') {
            tag = TagsEntry.new("AliasName\tFileName\t/^using AliasName = TypeName;$/;\"\n")

            assert_equal('TypeName', usingFromTagPatern(tag))
        }

        should('return nil when typedef extraction fails') {
            tag = TagsEntry.new("AliasName\tFileName\t/^using WrongAlias = TypeName;$/;\"\n")

            assert_equal(nil, usingFromTagPatern(tag))
        }
    }
end
