#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/DotDrawClass'

class DotDrawClassTestSuite < Test::Unit::TestCase
    context('DotDrawClass') {
        setup {
            @setting = Object.new
        }

        context('without namespace') {
            setup {
                @sut = DotDraw::Class.new(@setting, 'Foo')
            }

            should('read empty namespace') {
                assert_equal(nil, @sut.namespace(0))
            }
        }

        context('with namespace') {
            setup {
                @sut = DotDraw::Class.new(@setting, 'Space::Foo')
            }

            should('read namespace') {
                assert_equal('Space::', @sut.namespace(0))
                assert_equal(nil, @sut.namespace(1))
            }
        }

        context('with two namespaces') {
            setup {
                @sut = DotDraw::Class.new(@setting, 'Space1::Space2::Foo')
            }

            should('read namespace') {
                assert_equal('Space1::Space2::', @sut.namespace(0))
                assert_equal('Space1::', @sut.namespace(1))
                assert_equal(nil, @sut.namespace(2))
            }
        }
    }
end
