#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/DotDraw'

class DotDrawTestSuite < Test::Unit::TestCase
    include DotDraw::Utilities

    def assert_class_present clas, dotFile
        clas.gsub!('::', '_')

        assert_block("<#{dotFile}> doesn't contain class '#{clas}'") {
            !! dotFile.match(/^\s*#{Regexp.quote(clas)} \[$/)
        }
    end

    def assert_class_not_present clas, dotFile
        clas.gsub!('::', '_')

        assert_block("<#{dotFile}> contains class '#{clas}'") {
            ! dotFile.match(/^\s*#{Regexp.quote(clas)} \[$/)
        }
    end

    def assert_relation_present relation, dotFile
        relation.gsub!('::', '_')

        assert_block("<#{dotFile}> doesn't contain relation '#{relation}'") {
            !! dotFile.match(/^\s*#{Regexp.quote(relation)}$/)
        }
    end

    def assert_relation_not_present relation, dotFile
        relation.gsub!('::', '_')

        assert_block("<#{dotFile}> contains relation '#{relation}'") {
            ! dotFile.match(/^\s*#{Regexp.quote(relation)}$/)
        }
    end

    def assert_member_present field, type, dotFile
        dotField = escape("#{field} : #{type}")

        assert_block("<#{dotFile}> doesn't contain member '#{dotField}'") {
            !! dotFile.match(/#{Regexp.quote(dotField + '\l')}/)
        }
    end

    context('DotDraw') {
        setup {
            @sut = DotDraw.new
        }

        context('with [Foo, Bar]') {
            setup {
                @sut.clas['Foo']
                @sut.clas['Bar']
            }

            should('present classes on diagram') {
                assert_class_present('Foo', @sut.to_s)
                assert_class_present('Bar', @sut.to_s)
            }

            should('remove unrelated classes') {
                @sut.keepRelatedTo('Foo')

                assert_class_present('Foo', @sut.to_s)
                assert_class_not_present('Bar', @sut.to_s)
            }

            context('Bar inherits from Foo') {
                setup {
                    @sut.inheritance['Bar'].target = 'Foo'
                }

                should('present relation on diagram') {
                    assert_relation_present('Foo -> Bar', @sut.to_s)
                }

                should('keep related classes') {
                    @sut.keepRelatedTo('Foo')

                    assert_class_present('Foo', @sut.to_s)
                    assert_class_present('Bar', @sut.to_s)
                }
            }

            context('Bar contains Foo') {
                setup {
                    @sut.agregation['Bar'].target = 'Foo'
                }

                should('present relation on diagram') {
                    assert_relation_present('Bar -> Foo', @sut.to_s)
                }
            }

            [ 'shared_ptr<Foo>', 'unique_ptr<Foo>', 'Foo' ].each { |arg|
                context("Bar with #{arg}") {
                    setup {
                        @sut.clas['Bar'].member['field'].type = arg
                    }

                    should('present class member') {
                        assert_member_present('field', arg, @sut.to_s)
                    }

                    should('have no relations') {
                        assert_relation_not_present('Bar -> Foo', @sut.to_s)
                    }

                    context('after automatic member relations') {
                        setup {
                            @sut.autoCompleteMemberRelations
                        }

                        should('present relation on diagram') {
                            assert_relation_present('Bar -> Foo', @sut.to_s)
                        }
                    }
                }
            }

            [ 'weak_ptr<Foo>', 'const Foo&', 'Foo&' ].each { |arg|
                context("Bar with #{arg} after automatic member relations") {
                    setup {
                        @sut.clas['Bar'].member['field'].type = arg
                        @sut.autoCompleteMemberRelations
                    }

                    should('have no relations') {
                        assert_relation_not_present('Bar -> Foo', @sut.to_s)
                    }
                }
            }
        }

        context('with [Ns1::Foo, Ns1::Bar, Ns2::Baz]') {
            setup {
                @sut.clas['Ns1::Foo']
                @sut.clas['Ns1::Bar']
                @sut.clas['Ns2::Baz']
            }

            context('Bar with std::shared_ptr<Foo>, std::shared_ptr<Ns2::Baz>') {
                setup {
                    @sut.clas['Ns1::Bar'].member['field1'].type = 'std::shared_ptr<Foo>'
                    @sut.clas['Ns1::Bar'].member['field2'].type = 'std::shared_ptr<Ns2::Baz>'
                    @sut.autoCompleteMemberRelations
                }

                should('known target relation namespaces') {
                    assert_relation_present('Ns1::Bar -> Ns1::Foo', @sut.to_s)
                    assert_relation_present('Ns1::Bar -> Ns2::Baz', @sut.to_s)
                }
            }

            context('Bar with std::unique_ptr<::Ns2::Baz>') {
                setup {
                    @sut.clas['Ns1::Bar'].member['field'].type = 'std::unique_ptr<::Ns2::Baz>'
                    @sut.autoCompleteMemberRelations
                }

                should('refer to global namespace') {
                    assert_relation_present('Ns1::Bar -> Ns2::Baz', @sut.to_s)
                }
            }

            context('Bar with std::shared_ptr<Baz>') {
                setup {
                    @sut.clas['Ns1::Bar'].member['field'].type = 'std::shared_ptr<Baz>'
                    @sut.autoCompleteMemberRelations
                }

                should('gues member namespace') {
                    assert_relation_present('Ns1::Bar -> Ns2::Baz', @sut.to_s)
                }

                should('keep related classes') {
                    @sut.keepRelatedTo('Ns1::Bar')

                    assert_class_not_present('Ns1::Foo', @sut.to_s)
                    assert_class_present('Ns1::Bar', @sut.to_s)
                    assert_class_present('Ns2::Baz', @sut.to_s)
                }

                should('remove relations to unrelated classes') {
                    @sut.keepRelatedTo('Ns1::Foo')

                    assert_relation_not_present('Ns1::Bar -> Ns2::Baz', @sut.to_s)
                }
            }

            context('Bar with std::shared_ptr<BazTypedef>') {
                setup {
                    @sut.typedef['Ns1::BazTypedef'].type = 'Ns2::Baz'
                    @sut.clas['Ns1::Bar'].member['field'].type = 'std::shared_ptr<BazTypedef>'
                    @sut.autoResolveTypedefs
                }

                should('present typedef as classes on diagram') {
                    assert_class_present('Ns1::BazTypedef', @sut.to_s)
                }

                should('have inheritance relation from typedef') {
                    assert_relation_present('Ns2::Baz -> Ns1::BazTypedef', @sut.to_s)
                }

                should('remove relations to unrelated classes') {
                    @sut.keepRelatedTo('Ns1::Foo')

                    assert_relation_not_present('Ns2::Baz -> Ns1::BazTypedef', @sut.to_s)
                }

                context('after auto complete member relations') {
                    setup {
                        @sut.autoCompleteMemberRelations
                    }

                    should('have relation to typedef') {
                        assert_relation_present('Ns1::Bar -> Ns1::BazTypedef', @sut.to_s)
                    }
                }
            }

            context('Bar with BazPtrTypedef') {
                setup {
                    @sut.typedef['Ns1::BazPtrTypedef'].type = 'std::shared_ptr<Ns2::Baz>'
                    @sut.clas['Ns1::Bar'].member['field'].type = 'BazPtrTypedef'
                    @sut.autoResolveTypedefs
                    @sut.autoCompleteMemberRelations
                }

                should('typedef be related to extracted type') {
                    assert_relation_present('Ns2::Baz -> Ns1::BazPtrTypedef', @sut.to_s)
                }

                should('class be related to typedef') {
                    assert_relation_present('Ns1::Bar -> Ns1::BazPtrTypedef', @sut.to_s)
                }
            }

            context('Foo contains Baz, Bar contains Baz') {
                setup {
                    @sut.agregation['Ns1::Foo'].target = 'Ns2::Baz'
                    @sut.agregation['Ns1::Bar'].target = 'Ns2::Baz'
                    @sut.keepRelatedTo('Ns1::Foo')
                }

                should('discard unrelated classes') {
                    assert_class_present('Ns1::Foo', @sut.to_s)
                    assert_class_present('Ns2::Baz', @sut.to_s)
                    assert_class_not_present('Ns1::Bar', @sut.to_s)
                }

                should('discard unimportant relations') {
                    assert_relation_present('Ns1::Foo -> Ns2::Baz', @sut.to_s)
                    assert_relation_not_present('Ns1::Bar -> Ns2::Baz', @sut.to_s)
                }
            }
        }
    }
end

