#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/TagsParser'

require 'securerandom'
require 'fileutils'

class TagsParserTestSuite < Test::Unit::TestCase
    def self.setupDotDraw
        Proc.new {
            setup {
                @dotDraw = Object.new
                @dotClasses = Object.new
                @dotClass = Object.new
                @dotTypedefs = Object.new
                @dotTypedef = Object.new

                @dotDraw.expects(:clas).returns(@dotClasses).at_least(0)
                @dotDraw.expects(:typedef).returns(@dotTypedefs).at_least(0)

                @dotDraw.expects(:autoResolveTypedefs).at_least(0)
                @dotDraw.expects(:autoCompleteMemberRelations).at_least(0)
            }
        }
    end

    def parserWithContent fileName, content
        File.open(fileName, 'w') { |f|
            if content.kind_of? Array
                f.write(content.join("\n"))
            else
                f.write(content)
            end
        }

        TagsParser.new(fileName)
    end

    context('') {
        setup {
            @fileName = "/tmp/#{SecureRandom.hex}_tags"
            @yieldCount = 0
        }

        teardown {
            FileUtils.rm(@fileName) if File.exists?(@fileName)
        }

        context('TagsParser with missing file') {
            should('raise when creating') {
                assert_raise(Errno::ENOENT) {
                    @sut = TagsParser.new(@fileName)
                }
            }
        }

        context('TagsParser with empty file') {
            setup {
                @sut = parserWithContent(@fileName, '')
            }

            merge_block(&setupDotDraw)

            should('not yield') {
                @sut.each {
                    assert_equal(true, false, 'yields when it should\'nt')
                }
            }

            should('auto complete') {
                @seq = sequence('auto complete')

                @dotDraw.expects(:autoResolveTypedefs).in_sequence(@seq)
                @dotDraw.expects(:autoCompleteMemberRelations).in_sequence(@seq)

                @sut.toDraw(@dotDraw)
            }
        }

        context('TagsParser with class tag') {
            setup {
                @sut = parserWithContent(@fileName, "Align\tcppCommon/Source/Parallel/Detail/Align.hpp\t/^class Align$/;\"\tc\tnamespace:Common::Parallel::Detail")
            }

            merge_block(&setupDotDraw)

            should('yield tag entry') {
                @sut.each { |tag|
                    assert_equal(true, tag.kind_of?(TagsEntry))

                    @yieldCount += 1
                }

                assert_equal(1, @yieldCount)
            }

            should('add class to dot draw') {
                @dotClasses.expects(:[]).with('Align').returns(@dotClass).at_least(1)

                @sut.toDraw(@dotDraw)
            }
        }

        context('DotDraw') {
            merge_block(&setupDotDraw)

            context('TagsParser with typedef tag with namespace') {
                setup {
                    @sut = parserWithContent(@fileName, "CategoryQuery\tSource/Queries/CategoryQuery.hpp\t/^typedef StatusQuery CategoryQuery;$/;\"\tt\n")
                }

                should('add typedef to dot draw') {
                    @dotTypedefs.expects(:[]).with('CategoryQuery').returns(@dotTypedef).at_least(1)
                    @dotTypedef.expects(:type=).with('StatusQuery')

                    @sut.toDraw(@dotDraw)
                }
            }

            context('TagsParser with typedef tag with namespace') {
                setup {
                    @sut = parserWithContent(@fileName, "CategoryQuery\tSource/Queries/CategoryQuery.hpp\t/^typedef StatusQuery CategoryQuery;$/;\"\tt\tnamespace:MovieList\n")
                }

                should('add typedef with namespace to dot draw') {
                    @dotTypedefs.expects(:[]).with('MovieList::CategoryQuery').returns(@dotTypedef).at_least(1)
                    @dotTypedef.expects(:type=).with('StatusQuery')

                    @sut.toDraw(@dotDraw)
                }
            }
        }
    }
end
