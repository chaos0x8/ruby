#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'file/NoExt'

class MkPlaylist
    def self.usage param
        puts param.help
        puts
        puts 'Examples:'
        puts '# creates m3u playlist from *.mp3 files in directory \'A\' and \'B\''
        puts "  #{File.basename(__FILE__)} A B > playlist.m3u"
    end

    def self.header
      '#EXTM3U'
    end

    def self.serial
      '#MOCSERIAL: 512'
    end

    def self.buildExtInf fileName
      "#EXTINF:1,#{File.noExt(fileName)}"
    end

    def initialize
      @playedList = nil
    end

    def playedList= value
      @playedList = File.expand_path(value)
    end

    def excludePriorityList
      @excludePriorityList ||= Enumerator.new { |e|
        File.open(@playedList, 'r') { |f|
          f.each_line { |line|
            m = line.match(/^(\d){3,} (.*)$/)
            e << [Integer(m[1]), m[2]]
          }
        } if @playedList and File.exist?(@playedList)
      }.sort { |x, y| y[0] <=> x[0]
      }.collect { |x| x[1] }.to_a
    end

    def discard files, &block
      discardAmount = [ files.size / 4 * 3, excludePriorityList.size ].min
      toDiscard = excludePriorityList[0..discardAmount.to_i-1]

      files.reject { |fn|
        toDiscard.include?(fn)
      }.each(&block)
    end

    def generatePlaylist sourceDirectories
      Enumerator.new { |e|
        e << MkPlaylist::header
        e << MkPlaylist::serial

        files = sourceDirectories.collect { |sourceDir|
          Dir["#{sourceDir}/*.mp3"]
        }.flatten

        discard(files).each { |file|
          #e << MkPlaylist::buildExtInf(file)
          e << File.expand_path(file)
        }
      }.to_a
    end
end
