#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class MkPlaylistPlayedCount
    attr_accessor :filterExisting

    def initialize
      @filterExisting = nil

        ObjectSpace.define_finalizer(self) do
            if @file and ((@counterMap and not @counterMap.empty?) or @filterExisting)
                storeResult
            end
        end
    end

    def self.usage param
        puts param.help
        puts
        puts 'Examples:'
        puts '# increments counter of mp3 file'
        puts "  #{File.basename(__FILE__)} --file=played.txt 'my track.mp3'"
    end

    def file= value
        @file = File.expand_path(value)
    end

    def counterMap
        @counterMap ||= _counterMap
    end

    def _counterMap
        data = File.open(@file, 'r') { |f| f.read }

        result = Hash.new
        data.each_line("\n") do |line|
            m = line.match(/^(\d){3,} (.*)$/)
            result[m[2]] = Integer(m[1])
        end
        result
    rescue
        Hash.new
    end

    def incrementCounters *tracks
        raise RuntimeError.new('missing output file') unless @file
        raise RuntimeError.new('missing track') unless tracks

        tracks.each do |track|
            counterMap[track] ||= 0
            counterMap[track] += 1
        end
    end

    def storeResult
        dataToSave = counterMap
        dataToSave = filterData(dataToSave) if @filterExisting

        File.open(@file, 'w') do |f|
            dataToSave.each do |track, count|
                f.write("%03d #{track}\n" % count)
            end
        end
    end

    def filterData data
        data.select! { |track| File.exist?(track) }
        data
    end
    private :filterData
end
