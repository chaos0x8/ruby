#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'fileutils'
require 'shellwords'

require_relative '../lib/MkPlaylist'

class MkPlaylistTests < Test::Unit::TestCase
    MUSIC_DIR1 = '/tmp/MkPlaylistTests/music1'
    MUSIC_DIR1_FILE1 = "#{MUSIC_DIR1}/file1.mp3"
    MUSIC_DIR1_FILE2 = "#{MUSIC_DIR1}/file2.mp3"
    MUSIC_DIR2 = '/tmp/MkPlaylistTests/music2'
    MUSIC_DIR2_FILE1 = "#{MUSIC_DIR2}/file1.mp3"
    MUSIC_DIR2_FILE2 = "#{MUSIC_DIR2}/file2.mp3"

    PLAYED_LIST = '/tmp/MkPlaylistTests/played.txt'

    def assert_ge expected, actual
        assert_block "#{actual} expected to be greater or equal to #{expected}" do
            actual >= expected
        end
    end

    def assert_entry expected, actual
        assert_block "entry '#{expected}' not found in result list" do
            actual.include? expected
        end
    end

    def assert_entry_missing expected, actual
        assert_block "entry '#{expected}' found in result list" do
            not actual.include? expected
        end
    end

    context 'MkPlaylistTests' do
        setup do
            [ MUSIC_DIR1, MUSIC_DIR2 ].each { |dir| FileUtils.mkdir_p dir }
            [ MUSIC_DIR1_FILE1, MUSIC_DIR1_FILE2,
              MUSIC_DIR2_FILE1, MUSIC_DIR2_FILE2,
              "#{MUSIC_DIR1}/trash_file1", "#{MUSIC_DIR1}/trash_file2.mp4"].each do |file|
                FileUtils.touch(file) unless File.exist?(file)
            end

            @sut = MkPlaylist.new
        end

        should 'generate m3u header' do
            result = @sut.generatePlaylist [ MUSIC_DIR1 ]

            assert_ge(2, result.size)
            assert_equal('#EXTM3U', result[0])
            assert_equal('#MOCSERIAL: 512', result[1])
        end

        should 'contain list of mp3 files' do
            result = @sut.generatePlaylist [ MUSIC_DIR1, MUSIC_DIR2 ]

            assert_entry MUSIC_DIR1_FILE1, result
            assert_entry MUSIC_DIR1_FILE2, result
            assert_entry MUSIC_DIR2_FILE1, result
            assert_entry MUSIC_DIR2_FILE2, result
        end

        context 'with played list' do
            setup do
                @sut.playedList = PLAYED_LIST
            end

            teardown do
                FileUtils.rm PLAYED_LIST if File.exist?(PLAYED_LIST)
            end

            context 'which is empty' do
                setup do
                    File.open(PLAYED_LIST, 'w').close
                end

                should 'not exclude tracks' do
                    result = @sut.generatePlaylist [ MUSIC_DIR1, MUSIC_DIR2 ]

                    assert_entry MUSIC_DIR1_FILE1, result
                    assert_entry MUSIC_DIR1_FILE2, result
                    assert_entry MUSIC_DIR2_FILE1, result
                    assert_entry MUSIC_DIR2_FILE2, result
                end
            end

            context 'with data' do
                setup do
                    File.open(PLAYED_LIST, 'w') do |f|
                        f.write "001 #{MUSIC_DIR1_FILE1}\n"
                        f.write "004 #{MUSIC_DIR1_FILE2}\n"
                        f.write "003 #{MUSIC_DIR2_FILE1}\n"
                        f.write "002 #{MUSIC_DIR2_FILE2}\n"
                    end
                end

                should 'exclude recently played track' do
                    result = @sut.generatePlaylist [ MUSIC_DIR1, MUSIC_DIR2 ]

                    assert_entry_missing MUSIC_DIR1_FILE2, result
                end

                should 'exclude at most 3/4 of tracks' do
                    result = @sut.generatePlaylist [ MUSIC_DIR1, MUSIC_DIR2 ]

                    assert_entry_missing MUSIC_DIR1_FILE2, result
                    assert_entry_missing MUSIC_DIR2_FILE1, result
                    assert_entry_missing MUSIC_DIR2_FILE2, result
                    assert_entry MUSIC_DIR1_FILE1, result
                end
            end
        end
    end
end
