#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require 'fileutils'
require 'shellwords'

require_relative '../lib/MkPlaylistPlayedCount'

class TestMkPlaylistPlayerCount < Test::Unit::TestCase
    def assert_counter expectedValue, track
        data = File.open(@outputFile, 'r') { |f| f.read }

        assert_block "counter value of track #{track} doesn't equal #{expectedValue}" do
            assertResult = false
            data.each_line("\n") do |line|
                if m = line.match(/^(\d){3,} #{track}$/)
                    actualValue = m[1].to_i
                    assertResult = actualValue == expectedValue
                    break
                end
            end
            assertResult
        end
    end

    def assert_counter_count expected
        lines = File.open(@outputFile, 'r') { |f| f.readlines.size }
        assert_equal expected, lines
    end

    def self.shouldLeaveOnlyExistingFiles
        Proc.new {
            should('leave only existing file') {
                @sut.storeResult

                assert_counter 1, @existingTrack
                assert_counter_count 1
            }
        }
    end

    context('MkPlaylistPlayerCount') {
        setup {
            @outputFile = '/tmp/MkPlaylistTests/played.txt'
            @tracks = [ '/tmp/MkPlaylistTests/track 1.mp3',
                        '/tmp/MkPlaylistTests/track 2.mp3' ]
            @existingTrack = '/tmp/MkPlaylistTests/ex track 1.mp3'

            @tracks.each { |track| File.expects(:exist?).with(track).returns(false).at_least(0) }
            File.expects(:exist?).with(@existingTrack).returns(true).at_least(0)

            @sut = MkPlaylistPlayedCount.new
            @sut.file = @outputFile
        }

        teardown {
            Process.wait(Process.spawn("if [ -e #{Shellwords.escape(@outputFile)} ]; then \n\
                                            rm #{Shellwords.escape(@outputFile)} \n\
                                        fi"))
            assert_equal(0, $?.exitstatus)
        }

        should('increment track counter') {
            @sut.incrementCounters @tracks[0], @tracks[0], @tracks[1]
            @sut.storeResult

            assert_counter 2, @tracks[0]
            assert_counter 1, @tracks[1]
            assert_counter_count 2
        }

        context('with filter existing') {
            setup {
                @sut.filterExisting = true
            }

            context('after increment counters') {
                setup {
                    @sut.incrementCounters *@tracks, @existingTrack
                }

                merge_block(&shouldLeaveOnlyExistingFiles)
            }

            context('with existing output file') {
                setup {
                    File.open(@outputFile, 'w') { |f|
                        f.write "001 #{@tracks[0]}\n"
                        f.write "001 #{@existingTrack}\n"
                        f.write "001 #{@tracks[1]}\n"
                    }
                }

                merge_block(&shouldLeaveOnlyExistingFiles)
            }
        }
    }
end
