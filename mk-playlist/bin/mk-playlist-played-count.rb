#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'Param'
require 'Colorize'
require_relative '../lib/MkPlaylistPlayedCount'

begin
  PARAM = Param.new(Param::Arg::Option['--help', description: 'shows this message'],
                    Param::Arg::Option['--filter-existing', description: 'filter output file list leaving only existing files'],
                    Param::Arg::Value['--file', description: 'output file'])

  if PARAM['--help']
    MkPlaylistPlayedCount.usage(PARAM)
  else
    PARAM << Param::Mod::MandatoryArgs['--file']

    app = MkPlaylistPlayedCount.new
    app.file = PARAM['--file']
    app.filterExisting = PARAM['--filter-existing']
    app.incrementCounters(*PARAM.freeArgs)
  end
rescue RuntimeError => e
  puts "Exception: '#{e.to_s.red}'"
  MkPlaylistPlayedCount.usage(PARAM)
rescue => e
  puts "Exception: '#{e.to_s.red}'"
  puts e.backtrace.join("\n")
end
