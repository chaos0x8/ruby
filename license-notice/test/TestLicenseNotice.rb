#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/LicenseNotice'

class TestLiceseNotice < Test::Unit::TestCase
    include LicenseNotice

    def setOryginalComment value
        @fileComment.expects(:comment).returns(value).when(@st.is('oryginal')).at_least(0)
    end

    def setModifiedComment value
        @fileComment.expects(:comment).returns(value).when(@st.is('modified')).at_least(0)
    end

    context('TestLiceseNotice') {
        setup {
            self.expects(:puts).at_least(0)
        }

        setup {
            @filename = 'filename'
            @st = states('fileComment').starts_as('oryginal')

            @license = Object.new
            @license.expects(:adoptContent).with(@filename).returns('license').at_least(0)
            License.expects(:open).with('LICENSE').returns(@license).at_least(0)

            @fileComment = Object.new
            @fileComment.expects(:comment=).with('license').then(@st.is('modified')).at_least(0)
            @fileComment.expects(:content).returns('content').when(@st.is('modified')).at_least(0)
            FileComment.expects(:open).with(@filename).returns(@fileComment).at_least(0)

            @file = Object.new
            File.expects(:open).with(@filename, 'w').yields(@file).at_least(0)
        }

        should('write changes when license changes') {
            setOryginalComment 'org'
            setModifiedComment 'mod'

            @file.expects(:write).with('content')

            setCorrectDoxyLines(@filename)
        }

        should('not write changes when license not changed') {
            setOryginalComment 'org'
            setModifiedComment 'org'

            @file.expects(:write).never

            setCorrectDoxyLines(@filename)
        }
    }
end
