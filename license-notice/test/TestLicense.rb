#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/License'

class TestLicense < Test::Unit::TestCase
    def setFileDates filename, cyear, myear
        stat = Object.new
        File::Stat.expects(:new).returns(stat).at_least(0)
        stat.expects(:ctime).returns(DateTime.new(cyear)).at_least(0)
        stat.expects(:mtime).returns(DateTime.new(myear)).at_least(0)
    end

    context('TestLicense') {
        context('::open') {
            setup {
                @filename = 'filename'
            }

            should('raise when file doesn\'t exists') {
                File.expects(:exists?).with(@filename).returns(false).at_least(0)

                assert_raise(RuntimeError) {
                    License::open(@filename)
                }
            }

            context('with existing file') {
                setup {
                    File.expects(:exists?).with(@filename).returns(true).at_least(0)
                }

                should('raise when file is empty') {
                    File.expects(:size).with(@filename).returns(0).at_least(0)

                    assert_raise(RuntimeError) {
                        License::open(@filename)
                    }
                }

                context('with content') {
                    setup {
                        @file = Object.new
                        @content = 'content'

                        File.expects(:size).with(@filename).returns(@content.size).at_least(0)
                    }

                    should('read file content') {
                        File.expects(:open).with(@filename, 'r').yields(@file).at_least(0)
                        @file.expects(:read).returns(@content).at_least(0)

                        assert_equal('content', License::open(@filename).adoptContent(nil))
                    }
                }
            }
        }

        context('.adoptContent') {
            setup {
                fileComment = Object.new
                FileComment.expects(:open).returns(fileComment).at_least(0)

                fileComment.expects(:comment).returns('').at_least(0)
            }

            should('return oryginal content when #{year} not present') {
                assert_equal('{year}', License.new('{year}').adoptContent('filename'))
            }

            should('return oryginal content when file not passed') {
                assert_equal('#{year}', License.new('#{year}').adoptContent(nil))
            }

            should('replace #{year} with file year') {
                setFileDates('filename', 2012, 2012)

                assert_equal('2012', License.new('#{year}').adoptContent('filename'))
            }

            should('replace #{year} with file year range') {
                setFileDates('filename', 2000, 2011)

                assert_equal('2000 - 2011', License.new('#{year}').adoptContent('filename'))
            }

            should('keep whole content when replacing #{year}') {
                setFileDates('filename', 2001, 2001)

                assert_equal('| 2001 |', License.new('| #{year} |').adoptContent('filename'))
            }

            context('with existing comment in file') {
                setup {
                    fileComment = Object.new
                    FileComment.expects(:open).with('filename').returns(fileComment).at_least(0)

                    lines = ['# line1',
                             '# something 2007',
                             '# line2'].join("\n")
                    fileComment.expects(:comment).returns(lines).at_least(0)

                    setFileDates('filename', 2010, 2012)
                }

                should('include year in existing comment when placed in correct line') {
                    lines = [ '# new line1',
                              '# new something 2007 - 2012' ].join("\n")
                    assert_equal(lines,
                        License.new("# new line1\n# new something \#{year}").adoptContent('filename'))
                }

                should('not include year in existing comment when placed in wrong line') {
                    lines = [ '# new line1',
                              '# new line2',
                              '# new something 2010 - 2012' ].join("\n")
                    assert_equal(lines,
                        License.new("# new line1\n# new line2\n# new something \#{year}").adoptContent('filename'))
                }
            }
        }
    }
end
