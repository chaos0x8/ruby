#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/NewLines'

class TestNewLines < Test::Unit::TestCase
    context('TestNewLines') {
        [ "\n\nText\n\n\n", [ '', '', 'Text', '', '', '' ] ].each { |arg|
            context("with #{arg.class}") {
                setup {
                    @sut = NewLines.new(arg)
                }

                should('count lines at the beggining') {
                    assert_equal(2, @sut.frontNewLines)
                }

                should('count lines at the end') {
                    assert_equal(3, @sut.tailNewLines)
                }

                should('should change front new lines') {
                    @sut.frontNewLines = 4

                    assert_equal(4, @sut.frontNewLines)
                }

                should('should change tail new lines') {
                    @sut.tailNewLines = 1

                    assert_equal(1, @sut.tailNewLines)
                }

                if arg.class == String
                    should('keep content intact when removing new lines') {
                        @sut.frontNewLines = 0
                        @sut.tailNewLines = 0

                        assert_equal('Text', @sut.content)
                    }
                elsif arg.class == Array
                    should('keep content intact when removing new lines') {
                        @sut.frontNewLines = 0
                        @sut.tailNewLines = 0

                        assert_equal(['Text'], @sut.content)
                    }
                end

                should('convert to string with few new lines') {
                    @sut.frontNewLines = 3
                    @sut.tailNewLines = 3

                    assert_equal("\n\n\nText\n\n\n", @sut.to_s)
                }
            }
        }
    }
end
