#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/FileComment'

class TestFileComment < Test::Unit::TestCase
    def setFileContent *args
        @content = args.join("\n")
        @file.expects(:read).returns(@content).at_least(0)
    end

    context('TestFileComment') {
        setup {
            @file = Object.new

            File.expects(:exists?).returns(true).at_least(0)
            File.expects(:open).with(anything, 'r').yields(@file).at_least(0)
        }

        [ '.sh', '.rb' ].each { |ext|
            context("file with hash style comment (#{ext})") {
                setup {
                    @filename = 'filename.' + ext
                }

                context('with comment after ignore lines') {
                    setup {
                        setFileContent('#!/usr/bin/ruby',
                                       '',
                                       '# encoding utf-8',
                                       '',
                                       '# line1',
                                       '# line2',
                                       '',
                                       'def function; end',
                                       '', '')

                        @sut = FileComment::open(@filename)
                    }

                    should('read ruby comment') {
                        lines = [ '# line1',
                                  '# line2' ]

                        assert_equal(lines.join("\n"), @sut.comment)
                    }

                    should('return oryginal file content') {
                        assert_equal(@content, @sut.content)
                    }


                    [ { :id => 'A', :comment => '# new comment' },
                      { :id => 'B', :comment => '#    new comment' },
                      { :id => 'C', :comment => '     new comment    ' } ].each { |args|
                        should("change comment lines #{args[:id]}") {
                            lines = [ '#!/usr/bin/ruby',
                                      '',
                                      '# encoding utf-8',
                                      '',
                                      '# new comment',
                                      '',
                                      'def function; end',
                                      '', '' ]


                            @sut.comment = args[:comment]

                            assert_equal(lines.join("\n"), @sut.content)
                        }
                    }
                }

                context('without comment') {
                    setup {
                        setFileContent('')

                        @sut = FileComment::open(@filename)
                    }

                    should('place comment at the beginning') {
                        @sut.comment = '# new comment'

                        assert_equal('# new comment', @sut.content)
                    }

                    context('with ignore lines') {
                        setup {
                            setFileContent('#!/usr/bin/ruby',
                                           'def function; end')

                            @sut = FileComment::open(@filename)
                        }

                        should('place comment after ignore lines') {
                            lines = [ '#!/usr/bin/ruby',
                                      '',
                                      '# new comment',
                                      '',
                                      'def function; end']

                            @sut.comment = '# new comment'

                            assert_equal(lines.join("\n"), @sut.content)
                        }
                    }
                }
            }
        }

        [ '.hpp', '.cpp', '.h', '.c' ].each { |ext|
            context("file slash style comment (#{ext})") {
                setup {
                    @filename = 'filename.' + ext

                    setFileContent('/*!',
                                   '// line1',
                                   '',
                                   '// line2',
                                   '*/',
                                   'void function()')
                }

                should('read c++ comment') {
                    lines = [ '// line1',
                              '',
                              '// line2' ]

                    assert_equal(lines.join("\n"), FileComment::open(@filename).comment)
                }

                should('change c++ block comment') {
                    lines = [ '/*!',
                              ' *  new line1',
                              ' */',
                              '',
                              'void function()' ]

                    @sut = FileComment::open(@filename)
                    @sut.comment = 'new line1'

                    assert_equal(' *  new line1', @sut.comment)
                    assert_equal(lines.join("\n"), @sut.content)
                }
            }
        }

        [ '.vim' ].each { |ext|
            context("file quote style comment (#{ext})") {
                setup {
                    @filename = 'filename.' + ext
                }

                context('with code after comment') {
                    setup {
                        setFileContent('" line1',
                                       '" line2',
                                       '',
                                       'function()')
                    }

                    should('read ruby comment') {
                        lines = [ '" line1',
                                  '" line2' ]

                        assert_equal(lines.join("\n"), FileComment::open(@filename).comment)
                    }
                }

                context('with code before comment') {
                    setup {
                        setFileContent('function()',
                                       '" line1',
                                       '" line2')
                    }

                    should('return nil') {
                        assert_equal(nil, FileComment::open(@filename).comment)
                    }
                }
            }
        }
    }
end

