#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'FileComment'

class License
    def self.open filename
        raise "File #{filename} doesn't exists!" unless File.exists?(filename)
        raise "File #{filename} is empty" unless File.size(filename) > 0

        result = nil

        File.open(filename, 'r') { |f|
            result = License.new(f.read)
        }

        result
    end

    def initialize content
        @content = content
    end

    def adoptContent filename
        if filename and @content.match('#{year}')
            fs = File::Stat.new(filename)
            years = [ fs.ctime, fs.mtime ].collect { |dt| Integer(dt.strftime('%Y')) }

            splitContent = @content.split("\n")

            if comment = FileComment::open(filename).comment
                comment = comment.split("\n")
                comment.each_index { |i|
                    if i < splitContent.size and splitContent[i].match('#{year}')
                        if y = comment[i].match(/(\d{4})/)
                            years.push(Integer(y[1]))
                            break
                        elsif y = comment[i].match(/(\d{4}) - (\d{4})/)
                            years.push(Integer(y[1]))
                            years.push(Integer(y[2]))
                            break
                        end
                    end
                }
            end

            years = years.sort
            replaceString = if years.first == years.last
                years.first.to_s
            else
                "#{years.first} - #{years.last}"
            end

            return @content.gsub('#{year}', replaceString)
        end

        @content
    end
end
