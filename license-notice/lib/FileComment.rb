#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require_relative 'NewLines'

class FileComment
    module Detail
        def stripNewLines symbol, text
            tmp = NewLines.new(text)
            tmp.send(symbol, 0)
            tmp.content
        end
    end

    include FileComment::Detail

    def self.open filename
        result = nil

        File.open(filename, 'r') { |f|
            result = FileComment.new(f.read, File.extname(filename))
        }

        result
    end

    def initialize content, extension
        @content = content.split("\n")
        @extension = extension
        @trailingSpaces = NewLines.new(content).tailNewLines
        @blockComment = nil
        @ignoreEnded = nil

        extractComment = 'extractComment_' + @extension.gsub('.', '')
        self.send(extractComment.to_sym)
    end

    def comment
        return nil unless @commentStarted
        return @content[@commentStarted+1..@commentEnded-1].join("\n") if @blockComment
        @content[@commentStarted..@commentEnded].join("\n")
    end

    def comment= value
        symbol = 'commentOut_' + @extension.gsub('.', '')
        _value = self.send(symbol, value.split("\n"))

        _front = _back = nil

        if @commentStarted
            if @commentStarted > 0
                _front = @content[0..@commentStarted-1]
            else
                _front = Array.new
            end

            _back = @content[@commentEnded+1..@content.size-1]
        elsif @ignoreEnded
            _front = @content[0..@ignoreEnded]
            _back = @content[@ignoreEnded+1..@content.size-1]
        else
            _front = Array.new
            _back = @content
        end

        _front = stripNewLines(:tailNewLines=, _front)
        _back = stripNewLines(:frontNewLines=, _back)

        _value = NewLines.new(_value)
        _value.frontNewLines = ( _front.empty? ? 0 : 1 )
        _value.tailNewLines = ( _back.empty? ? 0: 1 )
        _value = _value.content

        @content = _front + _value + (_back || Array.new)
        extractComment = 'extractComment_' + @extension.gsub('.', '')
        self.send(extractComment.to_sym)
    end

    def content
        @content.join("\n") + "\n" * @trailingSpaces
    end

private
    def algorithm opts = Hash.new
        opts[:exclude] ||= Array.new
        opts[:exclude] = [ opts[:exclude] ] unless opts[:exclude].kind_of? Array

        @commentStarted = nil
        @commentEnded = nil

        @content.each_index{ |i|
            line = @content[i]

            if @commentStarted.nil? and opts[:exclude].index { |p| line.match(p) }
                @ignoreEnded = i
                next
            elsif @commentStarted.nil? and line.match(opts[:patern])
                @commentStarted = i
            elsif line.empty?
                next
            elsif line.match(opts[:patern])
                @commentEnded = i
                next
            else
                break
            end
        }

        @commentEnded ||= @content.size
    end

    def extractComment_hash
        algorithm(:patern => /^\s*#/, :exclude => [ /^#!/, /^# encoding/ ])
    end

    alias_method :extractComment_rb, :extractComment_hash
    alias_method :extractComment_sh, :extractComment_hash

    def extractComment_shash
        @commentStarted = nil
        @commentEnded = nil

        @content.each_index { |i|
            line = @content[i]

            if @commentStarted.nil? and line.match(/^\s*#{Regexp.quote('/*!')}/)
                @commentStarted = i
            elsif line.match(/#{Regexp.quote('*/')}$/)
                @commentEnded = i
                break
            end
        }

        @commentStarted = nil unless @commentEnded
        @blockComment = true
    end

    alias_method :extractComment_h, :extractComment_shash
    alias_method :extractComment_hpp, :extractComment_shash
    alias_method :extractComment_c, :extractComment_shash
    alias_method :extractComment_cpp, :extractComment_shash

    def extractComment_quote
        algorithm(:patern => /^\s*"/)
    end

    alias_method :extractComment_vim, :extractComment_quote

    def commentOutAlgorithm lines, comment
        lines.collect { |line|
            m = line.match(/^(\s*#{Regexp.quote(comment)})?(.*)$/)
            "#{comment} #{m[2].strip}".rstrip
        }
    end

    def commentOut_hash lines
        commentOutAlgorithm(lines, '#')
    end

    alias_method :commentOut_rb, :commentOut_hash
    alias_method :commentOut_sh, :commentOut_hash

    def commentOut_slash lines
        commentedOut = commentOutAlgorithm(lines, ' * ')
        [ '/*!' ] + commentedOut + [ ' */', '' ]
    end

    alias_method :commentOut_h, :commentOut_slash
    alias_method :commentOut_hpp, :commentOut_slash
    alias_method :commentOut_c, :commentOut_slash
    alias_method :commentOut_cpp, :commentOut_slash

    def commentOut_quote lines
        commentOutAlgorithm(lines, '"')
    end

    alias_method :commentOut_vim, :commentOut_quote
end
