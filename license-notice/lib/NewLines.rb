#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2015 - 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class NewLines
    attr_reader :content

    def initialize content
        @content = content
    end

    def frontNewLines
        symbol = "newLines_#{@content.class}".to_sym
        self.class.send(symbol, @content)
    end

    def tailNewLines
        symbol = "newLines_#{@content.class}".to_sym
        self.class.send(symbol, @content.reverse)
    end

    def frontNewLines= value
        setFrontNewLines_Any :previous => frontNewLines, :new => value, :nl => newLineCharacter
    end

    def tailNewLines= value
        setTailNewLines_Any :previous => tailNewLines, :new => value, :nl => newLineCharacter
    end

    def to_s
        symbol = "to_s_#{@content.class}"
        send(symbol)
    end

private
    def self.newLines_Any content, opts = Hash.new
        count = 0

        content.send(opts[:iterator]) { |v|
            break unless yield(v)

            count += 1
        }

        count
    end

    def self.newLines_String content
        newLines_Any(content, :iterator => :each_char) { |c| c == "\n" }
    end

    def self.newLines_Array content
        newLines_Any(content, :iterator => :each) { |c| c.empty? }
    end

    def setFrontNewLines_Any opts = Hash.new
        @content = opts[:nl] * opts[:new] + @content[opts[:previous]..@content.size-1]
    end

    def setTailNewLines_Any opts = Hash.new
        _end = @content.size-1
        @content = @content[0.._end - opts[:previous]] + opts[:nl] * opts[:new]
    end

    def newLineCharacter
        send("nl_#{@content.class}".to_sym)
    end

    def nl_String
        "\n"
    end

    def nl_Array
        [ '' ]
    end

    def to_s_String
        @content
    end

    def to_s_Array
        @content.join("\n")
    end
end
