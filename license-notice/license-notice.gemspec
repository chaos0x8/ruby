Gem::Specification.new { |s|
  s.name        = 'license-notice'
  s.version     = '0.0.4'
  s.date        = DateTime.now.strftime('%Y-%m-%d')
  s.summary     = "#{s.name} library"
  s.description = "Adds license comment to files"
  s.authors     = ["chaos0x8"]
  s.files       = Dir['lib/*.rb', 'bin/*.rb']
  s.executables = Dir['bin/*.rb'].collect { |x| File.basename(x) }
}
