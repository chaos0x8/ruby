#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'test/unit'
require 'mocha/setup'
require 'shoulda'

require_relative '../lib/TestSupport'

class TestTestSupportWith < Test::Unit::TestCase
    def value= val
        @value = val
    end

    def array= *args
        @array = [ *args ]
    end

    context('TestSupport/With') {
        with(:value= => 15) {
            should('set value') {
                assert_equal(15, @value)
            }
        }

        with(:array= => [1,2,3]) {
            should('set array') {
                assert_equal([1,2,3], @array)
            }
        }

        with(:value= => 3, :array= => 42) {
            should('set value + array') {
                assert_equal(3, @value)
                assert_equal([42], @array)
            }
        }
    }
end
