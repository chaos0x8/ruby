Gem::Specification.new { |s|
  s.name        = 'test-support'
  s.version     = '0.0.1'
  s.date        = File.mtime(__FILE__).strftime('%Y-%m-%d')
  s.summary     = "#{s.name} library"
  s.description = "Extra stuff for tests"
  s.authors     = ["chaos0x8"]
  s.files       = Dir['lib/**/*.rb']
}
