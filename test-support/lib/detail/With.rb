#!/usr/bin/ruby

# \author <https://github.com/chaos0x8>
# \copyright
# Copyright (c) 2016, <https://github.com/chaos0x8>
#
# \copyright
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# \copyright
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class Test::Unit::TestCase
    def self.with *args, &block
        argsText = args.collect { |o|
            if o.kind_of? Hash
                o.collect { |k, v| "#{k}(#{v.to_s})" }.join(', ')
            else
                o.to_s
            end
        }

        contextName = 'with ' + argsText.join(', ')

        context(contextName) {
            setup {
                args.each { |o|
                    if o.kind_of? Hash
                        o.each { |k, v|
                            if v.kind_of? Array
                                send(k, *v)
                            else
                                send(k, v)
                            end
                        }
                    else
                        send(o)
                    end
                }
            }

            merge_block(&block)
        }
    end
end
